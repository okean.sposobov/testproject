package dz.dz3_1.task3;

import dz.dz3_1.task2.Student;

import java.util.Arrays;
import java.util.Random;


public class Main {
    public static void main(String[] args) {
        StudentService studentService = new StudentService();
        interface Rnd {
            int[] getRandomGrades();
        }

        Rnd rnd = () -> {
            int[] rndGradesArray = new int[10];
            for (int i = 0; i < rndGradesArray.length; i++) {
                rndGradesArray[i] = new Random().nextInt(10) + 1;
            }
            return rndGradesArray;
        };

        Student[] students = new Student[4];
        for (int i = 0; i < students.length; i++) {
            students[i] = new Student();
            students[i].setGrades(rnd.getRandomGrades());
            students[i].setSurname("" + (char) (int) ((Math.random() * 26) + 65) + (char) (int) ((Math.random() * 26) + 97) + (char) (int) ((Math.random() * 26) + 97));
            System.out.println(students[i].getSurname());
            System.out.println(Arrays.toString(students[i].getGrades()) + "\t" + students[i].getAverageGrade());
        }

        System.out.println(studentService.bestStudent(students).getSurname() + " " + studentService.bestStudent(students).getAverageGrade());

        studentService.sortBySurname(students);
        for (Student st : students) {
            System.out.println(st.getSurname() + " " + st.getAverageGrade());
        }
    }
}
