package dz.dz3_1.task3;

import dz.dz3_1.task2.Student;

/**
 *  ДЗ 3 Часть 1
 *  Задача 3.
 *  Необходимо реализовать класс StudentService.
 * У класса должны быть реализованы следующие публичные методы:
 * ● bestStudent() — принимает массив студентов (класс Student из
 * предыдущего задания), возвращает лучшего студента (т.е. который
 * имеет самый высокий средний балл). Если таких несколько — вывести
 * любого.
 * ● sortBySurname() — принимает массив студентов (класс Student из
 * предыдущего задания) и сортирует его по фамилии.
 */

public class StudentService {
    public Student bestStudent(Student[] studentsArray) {
        Student bestStudent = studentsArray[0];
        if (studentsArray.length > 1) {     // поиск и возврат максимальной средней оценки
            for (int i = 1; i < studentsArray.length; i++) {
                bestStudent = bestStudent.getAverageGrade() < studentsArray[i].getAverageGrade() ?
                        studentsArray[i] : bestStudent;
            }
            return bestStudent;
        }
        return bestStudent;
    }

    public void sortBySurname(Student[] studentsArray) {
        if (studentsArray.length > 1) {
            for (int i = 0; i < studentsArray.length - 1; i++) {            //  Стандартная сортировка по полю Surname
                for (int k = i + 1; k < studentsArray.length; k++) {
                    if (studentsArray[i].getSurname().compareTo(studentsArray[k].getSurname()) > 0) {
                        Student tmp = studentsArray[i];
                        studentsArray[i] = studentsArray[k];
                        studentsArray[k] = tmp;
                    }
                }
            }
        }
    }
}
