package dz.dz3_1.task4;

public class Main {
    public static void main(String[] args) {
        TimeUnit timeUnit = new TimeUnit("10", "50", "20");
        timeUnit.printTime24();
        System.out.println();
        timeUnit.printTime12();
        System.out.println();
        timeUnit.addTime("15", "15", "10");
        timeUnit.printTime24();
        System.out.println();
        timeUnit = new TimeUnit("10", "50");
        timeUnit.printTime24();
        System.out.println();
        timeUnit = new TimeUnit("10");
        timeUnit.printTime24();
        System.out.println();
        timeUnit.printTime12();




    }
}
