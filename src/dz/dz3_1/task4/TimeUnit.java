package dz.dz3_1.task4;

import java.util.regex.Pattern;

/**
 * ДЗ 3 Часть 1
 * Задача 4.
 * Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
 * (необходимые поля продумать самостоятельно). Обязательно должны быть
 * реализованы валидации на входные параметры.
 * Конструкторы:
 * ● Возможность создать TimeUnit, задав часы, минуты и секунды.
 * ● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
 * должны проставиться нулевыми.
 * ● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
 * должны проставиться нулевыми.
 * Публичные методы:
 * ● Вывести на экран установленное в классе время в формате hh:mm:ss
 * ● Вывести на экран установленное в классе время в 12-часовом формате
 * (используя hh:mm:ss am/pm)
 * ● Метод, который прибавляет переданное время к установленному в
 * TimeUnit (на вход передаются только часы, минуты и секунды).
 */

public class TimeUnit {
    private String minutes;
    private String seconds;
    private String hours;

    private static final Pattern HOURS_PATTERN = Pattern.compile("[0-2]?[0-9]");
    private static final Pattern MINUTES_PATTERN = Pattern.compile("[0-5][0-9]");
    private static final Pattern SECONDS_PATTERN = Pattern.compile("[0-5][0-9]");

    public static boolean isValidHours(String hours) {
        return HOURS_PATTERN.matcher(hours).matches();
    }

    public static boolean isValidMinutes(String minutes) {
        return MINUTES_PATTERN.matcher(minutes).matches();
    }

    public static boolean isValidSeconds(String seconds) {  // можно было использовать MINUTES_PATTERN, но для единообразия так
        return SECONDS_PATTERN.matcher(seconds).matches();
    }

    public TimeUnit() {
        this.hours = "00";
        this.minutes = "00";
        this.seconds = "00";
    }

    public TimeUnit(String hours) {
        if (isValidHours(hours)) {
            this.hours = hours;
        } else {
            System.out.println("Неверный формат!");
        }
        this.minutes = "00";
        this.seconds = "00";
    }

    public TimeUnit(String hours, String minutes) {
        if (isValidHours(hours) && isValidMinutes(minutes)) {
            this.hours = hours;
            this.minutes = minutes;
        } else {
            System.out.println("Неверный формат!");
        }
        this.seconds = "00";
    }

    public TimeUnit(String hours, String minutes, String seconds) {
        if (isValidHours(hours) && isValidMinutes(minutes) && isValidSeconds(seconds)) {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        } else {
            System.out.println("Неверный формат!");
        }
    }

    public void printTime24() {
        System.out.print(hours + ":" + minutes + ":" + seconds);
    }

    public void printTime12() {
        int hours24 = Integer.parseInt(hours);
        int hours12 = hours24 % 12;
        if (hours24 / 12 == 0) {
            System.out.print(hours12 + ":" + minutes + ":" + seconds + " am");
        } else {
            System.out.print(hours12 + ":" + minutes + ":" + seconds + " pm");
        }
    }

    public void addTime(String hours, String minutes, String seconds) {
        int sumSeconds = Integer.parseInt(this.seconds) + Integer.parseInt(seconds);        // парсинг целых, операции для получения новых значений, преобразования в строки
        String newSeconds = String.valueOf(sumSeconds % 60);
        this.seconds = newSeconds.length() < 2 ? "0" + newSeconds : newSeconds;

        int sumMinutes = Integer.parseInt(this.minutes) + Integer.parseInt(minutes);
        int addedMinutes = sumMinutes + sumSeconds / 60;
        String newMinutes = String.valueOf(+addedMinutes % 60);
        this.minutes = newMinutes.length() < 2 ? "0" + newMinutes : newMinutes;

        int sumHours = Integer.parseInt(this.hours) + Integer.parseInt(hours);
        int addedHours = sumHours + addedMinutes / 60;
        this.hours = String.valueOf(addedHours % 24);
    }
}
