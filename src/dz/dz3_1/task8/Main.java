package dz.dz3_1.task8;

public class Main {
    public static void main(String[] args) {
        Atm atm1 = new Atm(60);
        System.out.println(atm1.rubToUsd(120));
        System.out.println(atm1.usdToRub(2));
        System.out.println(atm1.getCountAtm());
        Atm atm2 = new Atm(50);
        System.out.println(atm2.rubToUsd(120));
        System.out.println(atm2.usdToRub(2));
        System.out.println(atm2.getCountAtm());

    }
}
