package dz.dz3_1.task8;

/**
 * ДЗ 3 Часть 1
 * Задача 8.
 * Реализовать класс “банкомат” Atm.
 * Класс должен:
 * ● Содержать конструктор, позволяющий задать курс валют перевода
 * долларов в рубли и курс валют перевода рублей в доллары (можно
 * выбрать и задать любые положительные значения)
 * ● Содержать два публичных метода, которые позволяют переводить
 * переданную сумму рублей в доллары и долларов в рубли
 * ● Хранить приватную переменную счетчик — количество созданных
 * инстансов класса Atm и публичный метод, возвращающий этот счетчик
 * (подсказка: реализуется через static)
 */

public class Atm {
    private final double rubPerUsd;
    private final double usdPerRub;
    private static int countAtm;

    public Atm(double rubPerUsd) {
        this.rubPerUsd = rubPerUsd;
        this.usdPerRub = 1 / rubPerUsd;
        countAtm++;
    }

    public double rubToUsd(double rub) {
        return rub / rubPerUsd;
    }

    public double usdToRub(double usd) {
        return usd / usdPerRub;
    }

    public int getCountAtm() {
        return countAtm;
    }
}
