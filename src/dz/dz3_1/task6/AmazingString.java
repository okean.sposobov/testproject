package dz.dz3_1.task6;

/**
 *  ДЗ 3 Часть 1
 *  Задача 6.
 *  Необходимо реализовать класс AmazingString, который хранит внутри себя
 * строку как массив char и предоставляет следующий функционал:
 * Конструкторы:
 * ● Создание AmazingString, принимая на вход массив char
 * ● Создание AmazingString, принимая на вход String
 * Публичные методы (названия методов, входные и выходные параметры
 * продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
 * не прибегая к переводу массива char в String и без использования стандартных
 * методов класса String.
 * ● Вернуть i-ый символ строки
 * ● Вернуть длину строки
 * ● Вывести строку на экран
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается массив char). Вернуть true, если найдена и false иначе
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается String). Вернуть true, если найдена и false иначе
 * ● Удалить из строки AmazingString ведущие пробельные символы, если
 * они есть
 * ● Развернуть строку (первый символ должен стать последним, а
 * последний первым и т.д.)
 */

public class AmazingString {
    private final char[] sch;

    public AmazingString(char[] ch) {
        sch = new char[ch.length];
        for (int i = 0; i < sch.length; i++) {
            sch[i] = ch[i];
        }
    }

    public AmazingString(String str) {
        sch = str.toCharArray();
//        sch = new char[str.length()];
//        str.getChars(0, str.length(), sch, 0);

    }

    public char[] getChars() {
        return sch;
    }

    public char getCharAt(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0 || i > sch.length - 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return sch[i];
    }

    public int getLength() {
        return sch.length;
    }

    public void printString() {
        for (char c : sch) {
            System.out.print(c);
        }
    }

    public boolean isSubstring(char[] ch) {
        if (ch.length > sch.length) {
            return false;
        }

        for (int i = 0; i < sch.length - ch.length + 1; i++) {  // посимвольный перебор и сравнение
            for (int k = 0; k < ch.length; k++) {
                if (sch[i + k] != ch[k]) {      //  сброс цикла сравнения элементов при неравенстве элементов
                    break;
                }
                if (sch[i + k] == ch[k] && k == ch.length - 1) {   // true , если сравнение полностью завершено с совпадением с подстокой
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isSubstring(String str) {
        if (str.length() > sch.length) {
            return false;
        }
        char[] ch = new AmazingString(str).getChars();  // из строки получить массив char

        return isSubstring(ch);
    }

    public char[] trimLeading() {
        char[] spaces = {' ', '\n', '\t'}; // пробельные
        int k, i;
        for (i = 0; i < sch.length; i++) {
            for (k = 0; k < spaces.length; k++) {   // посимвольное сравнение
                if (sch[i] == spaces[k]) { // при совпадении с пробельными сброс и переход к следуюшиму [i] элементу основного массива
                    break;
                }
            }
            if (k == spaces.length) {       // пробельных не найдено
                break;
            }
        }

        if (i <= sch.length - 1) {          //  i-й  - первый не пробельный, в массиве пробельные не все
            char[] schTrim = new char[sch.length - i]; // длина массива без ведущих прбельных
            int t = 0;
            for (int j = i; j < sch.length; j++) {
                schTrim[t++] = sch[j];
            }
            return schTrim;
        } else {
            return new char[0]; // если все пробельные
        }
    }

    public char[] revers() {
        char[] reversed = new char[sch.length];
        for (int i = 0; i < sch.length; i++) {
            reversed[i] = sch[sch.length - 1 - i];
        }
        return reversed;
    }

}
