package dz.dz3_1.task6;

public class Main {
    public static void main(String[] args) {
        AmazingString amazingString = new AmazingString("asdfghjkl");
        System.out.println(amazingString.isSubstring(new char[]{'g', 'h', 'h', 'j'}));
        System.out.println(amazingString.isSubstring(new char[]{'g', 'h', 'j'}));
        System.out.println(amazingString.isSubstring(new char[]{'a', 'a'}));
        System.out.println(amazingString.isSubstring(new char[]{'j', 'k', 'l'}));
        System.out.println(amazingString.isSubstring(new char[]{'j', 'k', 'l', 'l'}));
        System.out.println(amazingString.isSubstring(new char[]{'l'}));
        System.out.println(amazingString.isSubstring("ddfg"));
        System.out.println(amazingString.isSubstring("dfg"));
        System.out.println(amazingString.isSubstring(new char[]{'o'}));

        System.out.println(amazingString.revers());

        amazingString = new AmazingString("asd");
        System.out.println(amazingString.isSubstring(new char[]{'a', 's', 'd', 'f'}));
        System.out.println(amazingString.revers());

        amazingString = new AmazingString("  \t l   \n    \tasd  dfg dgd  i");
        System.out.println(amazingString.trimLeading());

        char[] chars = new char[]{' ', '\n', 'o', 'p'};
        amazingString = new AmazingString(chars);
        System.out.println(amazingString.trimLeading());

        amazingString = new AmazingString(new char[]{' ', '\n', '\t'});
        System.out.println("______");
        System.out.println(amazingString.trimLeading());
        System.out.println("______");

        amazingString = new AmazingString("asdfghjkl");
        amazingString.printString();
        System.out.println();
        System.out.println(amazingString.getCharAt(3));
        System.out.println(amazingString.getLength());



    }
}
