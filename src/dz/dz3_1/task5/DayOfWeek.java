package dz.dz3_1.task5;

/**
 *  ДЗ 3 Часть 1
 *  Задача 5.
 *  Необходимо реализовать класс DayOfWeek для хранения порядкового номера
 * дня недели (byte) и названия дня недели (String).
 * Затем в отдельном классе в методе main создать массив объектов DayOfWeek
 * длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
 * Sunday) и вывести значения массива объектов DayOfWeek на экран.
 * Пример вывода:
 * 1 Monday
 * 2 Tuesday
 * …
 * 7 Sunday
 */

public class DayOfWeek {
    private byte numberOfDay;
    private String day;

    public DayOfWeek(byte numberOfDay, String day) {
        this.numberOfDay = numberOfDay;
        this.day = day;
    }

    public String getDayOfWeek() {
        return numberOfDay + " " + day;
    }

}
