package dz.dz3_1.task5;

public class Main {
    public static void main(String[] args) {
        DayOfWeek[] dayOfWeek = new DayOfWeek[7];
        dayOfWeek[0] = new DayOfWeek((byte) 1, "Monday");
        dayOfWeek[1] = new DayOfWeek((byte) 2, "Tuesday");
        dayOfWeek[2] = new DayOfWeek((byte) 3, "Wednesday");
        dayOfWeek[3] = new DayOfWeek((byte) 4, "Thursday");
        dayOfWeek[4] = new DayOfWeek((byte) 5, "Friday");
        dayOfWeek[5] = new DayOfWeek((byte) 6, "Saturday");
        dayOfWeek[6] = new DayOfWeek((byte) 7, "Sunday");

        for(int i =0;i<dayOfWeek.length;i++){
            System.out.println(dayOfWeek[i].getDayOfWeek());
        }
    }
}
