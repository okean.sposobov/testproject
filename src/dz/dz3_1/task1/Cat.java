package dz.dz3_1.task1;

/**
 *  ДЗ 3 Часть 1
 *  Задача 1.
 *  Необходимо реализовать класс Cat.
 * У класса должны быть реализованы следующие приватные методы:
 * ● sleep() — выводит на экран “Sleep”
 * ● meow() — выводит на экран “Meow”
 * ● eat() — выводит на экран “Eat”
 * И публичный метод:
 * status() — вызывает один из приватных методов случайным образом.
 */

public class Cat {
    public void status() {
        switch ((int) (Math.random() * 3)) {
            case 0 -> sleep();
            case 1 -> meow();
            case 2 -> eat();
        }
    }

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }
}
