package dz.dz3_1.task7;

public class Main {
    public static void main(String[] args) {
        System.out.println(TriangleChecker.isTriangle(1,1,2));
        System.out.println(TriangleChecker.isTriangle(1,2,2));
        System.out.println(TriangleChecker.isTriangle(1,1,1));
        System.out.println(TriangleChecker.isTriangle(3,1,1));
        System.out.println(TriangleChecker.isTriangle(0,1,2));

    }
}
