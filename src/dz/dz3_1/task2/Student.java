package dz.dz3_1.task2;

import java.util.Arrays;

/**
 *  ДЗ 3 Часть 1
 *  Задача 2.
 *  Необходимо реализовать класс Student.
 * У класса должны быть следующие приватные поля:
 * ● String name — имя студента
 * ● String surname — фамилия студента
 * ● int[] grades — последние 10 оценок студента. Их может быть меньше, но
 * не может быть больше 10.
 * И следующие публичные методы:
 * ● геттер/сеттер для name
 * ● геттер/сеттер для surname
 * ● геттер/сеттер для grades
 * ● метод, добавляющий новую оценку в grades. Самая первая оценка
 * должна быть удалена, новая должна сохраниться в конце массива (т.е.
 * массив должен сдвинуться на 1 влево).
 * ● метод, возвращающий средний балл студента (рассчитывается как
 * среднее арифметическое от всех оценок в массиве grades)
 */

public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];


    public Student() {
        Arrays.fill(this.grades, 0);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public double getAverageGrade() {
        int count = 0;
        double sum = 0.0;
        for (int i = grades.length - 1; i >= 0; i--) {
            if (grades[i] <= 0) {
                continue;
            }
            sum += grades[i];
            count++;
        }
        if (count == 0) {
            return 0;
        } else {
            return sum / count;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] newGrades) {
        Arrays.fill(grades, 0);
        addGrades(newGrades);
    }

    public void addGrade(int addedGrade) {
        System.arraycopy(grades, 1, grades, 0, grades.length - 1); // смещение на 1 позицию влево копированием
        grades[grades.length - 1] = addedGrade;
    }

    public void addGrades(int[] addedGradesArray) {
        if (addedGradesArray.length < grades.length) {
            System.arraycopy(grades, addedGradesArray.length, grades, 0, grades.length - addedGradesArray.length);  // сещение влево на длину добавляемого входящего массива копированием
            System.arraycopy(addedGradesArray, 0, grades, grades.length - addedGradesArray.length, addedGradesArray.length);    // добавление входящего массива на освобождённое место
        } else {
            System.arraycopy(addedGradesArray, addedGradesArray.length - grades.length, grades, 0, grades.length); // полное замещение элементов входящим массивом отсчитав с конца
        }
    }

}
