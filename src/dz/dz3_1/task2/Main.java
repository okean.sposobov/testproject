package dz.dz3_1.task2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        student.setGrades(new int[]{0, 3, 0, 1, 0});
        System.out.println(Arrays.toString(student.getGrades()));
        System.out.println(student.getAverageGrade());
        System.out.println();

        student.addGrade(5);
//        student.addGrade(7);
        System.out.println(Arrays.toString(student.getGrades()));
        System.out.println(student.getAverageGrade());
        System.out.println();

        student.addGrades(new int[]{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 7, 7, 8});
        System.out.println(Arrays.toString(student.getGrades()));
        System.out.println();

        student.addGrade(5);
        System.out.println(Arrays.toString(student.getGrades()));
        System.out.println(student.getAverageGrade());
        System.out.println();

        student.setGrades(new int[]{5, 5, 5, 5, 5, 6, 6, 6, 6, 6,});
        System.out.println(Arrays.toString(student.getGrades()));
        System.out.println(student.getAverageGrade());


    }
}
