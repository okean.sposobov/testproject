package dz.dz3_2;

/**
 * Класс электронной очереди. Направляет посетителя к библиотекарю с наименьшей очередью.
 * Если в очереди(library.getMaxLibrarianVisitorDeque().getCurrentDequeSize()) у библиотекаря больше 1
 * (или сколько указать в условии метода use() ) посетителей и количество работающих библиотекарей меньше максимального в библиотеке,
 * то электронная очередь вызывает одного библиотекаря.
 */

class QueueManagementSystem {
    Library library;

    QueueManagementSystem(Library library) {
        this.library = library;
    }

    public void use(Visitor visitor) {
        if (library.isOpen()) {
            Library.LibrarianVisitorDequeActions minVisitorDeque = library.getMinLibrarianVisitorDeque();
            minVisitorDeque.addVisitorToDeque(visitor);
            if (library.getMaxLibrarianVisitorDeque().getCurrentDequeSize() > 1 && library.getLibrarians().size() <= library.getMaxLibrarians()) { // очередь к библиотекарю боьше 1 (или указать) и количество библиотекарей меньше максимального
                library.callLibrarian();
            }
        } else {
            System.out.println("Библиотека закрыта.");
        }
    }
}
