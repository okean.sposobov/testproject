package dz.dz3_2;


/**
 *  Первая часть решения, пункты 1-4 задачи.
 *
 */

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Library library = new Library();
        library.fillLibrary(3);
        library.getBookArray().forEach(System.out::println);

        library.open();

        //  Добавление новой книги
        System.out.println();
        System.out.println("Добавление новой книги ");
        System.out.println("Добавление новой книги Book0");
        Book book0 = new Book("Author0", "Book0");
        library.addNewBook(book0);
        Book book22 = new Book("Author22", "Book22");
        System.out.println("Добавление новой книги Book22");
        library.addNewBook(book22);
        Visitor visitor = new Visitor("Visitor");
        library.getQueueManagementSystem().use(visitor);
        Thread.sleep(1000);     // Иначе главный поток конкурирует с Librarian0
        Book book = visitor.getBook();
        System.out.println("Добавление одолженной книги " + book);
        library.addNewBook(book);
        System.out.println(book);
        System.out.println("\n____________________");

        //  Удаление книги
        System.out.println("Удаление книги");
        library.deleteBookByName("Book33");
        library.deleteBookByName("Book0");      //  Может быть совпвдение
        library.deleteBookByName(book.getBookName());   //
        System.out.println("\n____________________");


        //  Найти и вернуть книгу по названию
        System.out.println("Найти и вернуть книгу по названию");
        String findBookByName = "Book1";
        System.out.println("Поиск книги " + findBookByName + "\n" + library.findBookByName(findBookByName));
        System.out.println("\n____________________");

        //  Найти и вернуть список книг по автору.
        System.out.println("Найти и вернуть список книг по автору.");
        System.out.println("Author1");
        System.out.println(library.findBooksByAuthor("Author1"));
        System.out.println("\n____________________");

        library.close();
    }
}
