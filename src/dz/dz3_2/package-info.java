package dz.dz3_2;
/**
 *  В каждом классе есть общее описание его работы.
 *
 *  class Library содержит private class Librarian implements LibrarianVisitorDequeActions, Runnable{} - библиотекарь,
 *  экземпляр которого работает в одельном потоке.
 *
 *  class QueueManagementSystem - электронная очередь в главном потоке.
 *  class Visitor - посетитель.
 *  class Book - книга.
 *
 *
 *
 */
