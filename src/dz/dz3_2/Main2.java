package dz.dz3_2;

/**
 * Вторая часть решения
 */

import java.util.ArrayList;
import java.util.List;

public class Main2 {
    public static void main(String[] args) throws InterruptedException {
        /**
         *  Механизм одалживания(со всеми условиями) книги посетителю с многопоточной реализацией.
         */
        Library library = new Library();
        library.fillLibrary(11);
        List<Visitor> visitorList = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            visitorList.add(new Visitor("Visitor" + i));
        }

        //  Запуск посетителей в библиотеку
        for (int i = 0; i < 14; i++) {
            System.out.println("День " + i);
            library.open();
            for (Visitor visitor : visitorList) {
                library.getQueueManagementSystem().use(visitor);
            }
            Thread.sleep(600);
            library.close();
        }


//  Список книг с оценками, нужно подождать окончания работы с посетителями
        Thread.sleep(1000);
        System.out.println();
        System.out.println();

        library.open();

        System.out.println("Список книг с оценками");
        library.getScoredBooks().forEach((scoredBook, score) -> System.out.println(scoredBook + "   " + score));
        Library.LibrarianVisitorDequeActions librarianVisitorDequeActions = library.getLibrarians().get(0);
        System.out.println("Оценки книг по названию");
        System.out.println("Book0  " + librarianVisitorDequeActions.getBookScoreByName("Book0"));
        System.out.println("Book1  " + librarianVisitorDequeActions.getBookScoreByName("Book1"));
        System.out.println("Book2  " + librarianVisitorDequeActions.getBookScoreByName("Book2"));
        System.out.println("Book3  " + librarianVisitorDequeActions.getBookScoreByName("Book3"));

        library.close();

    }
}
