package dz.dz3_2;

/**
 * Класс книги с минимальным набором полей (author, bookName)
 *
 */

public class Book {
    private String author;
    private final String bookName;

    Book(String author, String bookName){
        this.author = author;
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public String getBookName() {
        return bookName;
    }

    @Override
    public boolean equals(Object book) {
        if (book == this) {
            return true;
        }
        if (book == null || book.getClass() != this.getClass()) {
            return false;
        }
        Book testBook = (Book) book;
        return this.getBookName().compareTo(testBook.getBookName()) == 0;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", bookName='" + bookName + '\'' +
                '}';
    }
}
