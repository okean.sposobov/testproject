package dz.dz3_2;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;

/**
 * Класс Библиотеки.
 * <p>
 * Функционал из первой части задания (пункты 1-4) реализованы в самой библиотеке,
 * остальной функционал из второй части задачи реализован через библиотекарей, каждый обслуживает свою очередь посетителей в одельном потоке.
 * {@link Librarian} class Librarian implements LibrarianVisitorDequeActions, Runnable - приватный класс библиотекаря,
 * для взаимодействия с его функционалом используется интерфейс LibrarianVisitorDequeActions.
 * Если посетитель без книги, значит он пришёл её одолжить и наоборот.
 * Библиотекарь обслуживает посетителя методом serve() класса Librarian.
 */

public class Library {
    private CopyOnWriteArrayList<Book> bookArray;
    private ConcurrentHashMap<String, String> scoredBooks;
    private CopyOnWriteArrayList<String> borrowedBooks;

    private CopyOnWriteArrayList<String> borrowersId;
    private ConcurrentHashMap<String, Visitor> idToRegisteredVisitor;
    private ConcurrentHashMap<Visitor, String> registeredVisitorToId;
    private volatile QueueManagementSystem queueManagementSystem;
    private CopyOnWriteArrayList<Librarian> librarians;
    private CopyOnWriteArrayList<Thread> librarianThreads;
    private ExecutorService librariansPool;
    private static volatile boolean libraryIsOpen;
    private int maxLibrarians;


    public Library() {
        bookArray = new CopyOnWriteArrayList<>();
        scoredBooks = new ConcurrentHashMap<>();
        borrowedBooks = new CopyOnWriteArrayList<>();
        borrowersId = new CopyOnWriteArrayList<>();
        idToRegisteredVisitor = new ConcurrentHashMap<>();
        registeredVisitorToId = new ConcurrentHashMap<>();
        queueManagementSystem = new QueueManagementSystem(this);
        librarians = new CopyOnWriteArrayList<>();
        librarianThreads = new CopyOnWriteArrayList<>();
//        librariansPool = Executors.newWorkStealingPool();
//        libraryIsOpen = true;
//        callLibrarian();
        maxLibrarians = 4; // если 4 , то всего 5, счёт с 0
    }

    public boolean isOpen() {
        return libraryIsOpen;
    }

    public void open() {
        System.out.println("Библиотека открыта.");
        librariansPool = Executors.newWorkStealingPool();
        libraryIsOpen = true;
        callLibrarian();
    }

    public void close() throws InterruptedException {
        libraryIsOpen = false;
        Thread.sleep(700);
        librarianThreads.forEach(Thread::interrupt);
        librarianThreads.clear();
        librariansPool.shutdown();
        librarians.clear();

        Librarian.count = 0;
        System.out.println("Библиотека закрыта.");
    }

    public CopyOnWriteArrayList<String> getBorrowedBooks() {
        return borrowedBooks;
    }

    public QueueManagementSystem getQueueManagementSystem() {
        return queueManagementSystem;
    }

    public CopyOnWriteArrayList<Book> getBookArray() {
        return bookArray;
    }

    public void fillLibrary(int authorNumber) {
        int count = 0;
        for (int i = 0; i <= authorNumber; i++) {
            int rnd = new Random().nextInt(3) + 1;
            for (int k = 0; k < rnd; k++) {
                bookArray.add(new Book("Author" + i, "Book" + count));
                count++;
            }
        }
        System.out.println("Всего авторов: " + authorNumber + ", всего книг: " + count);
    }

    public boolean addNewBook(Book book) {
        if (!bookArray.contains(book)) {
            bookArray.add(book);
            System.out.println("Новая книга добавлена в библиотеку.");
            return true;
        } else {
            System.out.println("Такая книга уже есть в библиотеке.");
            return false;
        }
    }

    public boolean deleteBookByName(String bookName) {
        int index = bookArray.indexOf(new Book(null, bookName));
        if (index < 0) {
            System.out.println("Книга \"" + bookName + "\" не найдена в библиотеке.");
            return false;
        }
        if (!borrowedBooks.contains(bookName)) {
            System.out.println("Книга \"" + bookName + "\" удалена из библиотеки.");
            return bookArray.remove(index) != null;
        } else {
            System.out.println("Книга \"" + bookName + "\" одолжена и не может быть удалена из библиотеки.");
            return false;
        }

    }


    public ArrayList<String> findBooksByAuthor(String author) {
        ArrayList<String> booksListByAuthor = new ArrayList<>();
        for (Book book : bookArray) {
            if (book.getAuthor().equals(author)) {
                booksListByAuthor.add(book.getBookName());
            }
        }
        return booksListByAuthor;
    }

    public Book findBookByName(String bookName) {
        Book book = new Book(null, bookName);
        int index = bookArray.indexOf(book);
        if (index >= 0) {
            return bookArray.get(index);
        } else {
            System.out.println("Книги " + bookName + " нет.");
            return null;
        }
    }

    public ConcurrentHashMap<String, String> getScoredBooks() {
        return scoredBooks;
    }

    private synchronized String generateId() {
        LocalDateTime lDT = LocalDateTime.now();
        try {
            Thread.sleep(97);
        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
        }
        return "" + lDT.getYear() + lDT.getMonthValue() + lDT.getDayOfMonth() + lDT.getHour() + lDT.getMinute() + lDT.getSecond() + lDT.getNano();
    }


    public CopyOnWriteArrayList<Librarian> getLibrarians() {
        return librarians;
    }

    public void callLibrarian() {
        Librarian librarian = new Librarian();
        Thread librarianThread = new Thread(librarian);
        librariansPool.submit(librarianThread);
//        librarianThread.start();
        librarianThreads.add(librarianThread);
        librarians.add(librarian);
        System.out.println("вызван библиотекарь " + librarian.NAME);
    }

    private final Comparator<Library.Librarian> librarianQueueComparator = Comparator.comparingInt(Library.Librarian::getCurrentDequeSize);

    public LibrarianVisitorDequeActions getMinLibrarianVisitorDeque() {
        return Collections.min(librarians, librarianQueueComparator);
    }

    public LibrarianVisitorDequeActions getMaxLibrarianVisitorDeque() {
        return Collections.max(librarians, librarianQueueComparator);
    }

    public int getMaxLibrarians() {
        return maxLibrarians;
    }

    interface LibrarianVisitorDequeActions {
        void addVisitorToDeque(Visitor visitor);

        int getCurrentDequeSize();

        LibrarianVisitorDequeActions getLibrarian();

        boolean isRegisteredVisitor(Visitor visitor);

        String getRegisteredVisitorId(Visitor registeredVisitor);

        double getBookScoreByName(String bookName);
    }

    private class Librarian implements LibrarianVisitorDequeActions, Runnable {

        BlockingDeque<Visitor> visitorDeque;
        public final String NAME;
        static int count;

        Librarian() {
            this.visitorDeque = new LinkedBlockingDeque<>();
            this.NAME = "Librarian" + count++;
        }


        @Override
        public LibrarianVisitorDequeActions getLibrarian() {
            return this;
        }


        private String registerVisitor(Visitor visitor) {
//        System.out.println("registerVisitor" + visitor.getName());
            if (visitor != null) {
                if (!isRegisteredVisitor(visitor)) {
                    String id = generateId();
                    Visitor registeredVisitor = new Visitor(visitor);
                    idToRegisteredVisitor.put(id, registeredVisitor);
                    registeredVisitorToId.put(registeredVisitor, id);
                    if (visitor.setIdByLibrary(getLibrarian(), registeredVisitor)) {
                        System.out.println(Thread.currentThread().getName() + ": Посетитель " + visitor.getName() + " зарегистрирован в библиотеке. Идентификатор: " + visitor.getId());
                        return id;
                    } else {
                        System.out.println(Thread.currentThread().getName() + ": Невозможно зарегистрировать посетителя " + visitor.getName() + ".");
                        return null;
                    }
                } else {
                    System.out.println(Thread.currentThread().getName() + ": Зарегистрированный посетитель " + visitor.getName() + ".");
                    return visitor.getId();
                }
            } else {
                System.out.println(Thread.currentThread().getName() + ": Невозможно зарегистрировать данного посетителя (null).");
                return null;
            }
        }

        public boolean isRegisteredVisitor(Visitor visitor) {
            return idToRegisteredVisitor.containsValue(visitor);
        }

        public String getRegisteredVisitorId(Visitor registeredVisitor) {
            return registeredVisitorToId.get(registeredVisitor);
        }


        public boolean returnBook(Visitor visitor) {
            return returnBook(visitor, 0);
        }

        public boolean returnBook(Visitor visitor, int score) {
            Visitor registeredVisitor = idToRegisteredVisitor.get(registerVisitor(visitor));
            if (registeredVisitor != null && visitor.getBook() != null && registeredVisitor.getBook() != null && registeredVisitor.getBook().equals(visitor.getBook())) {
                setBookScore(visitor, score);
                borrowersId.remove(registeredVisitorToId.get(registeredVisitor));
                borrowedBooks.remove(registeredVisitor.getBook().getBookName());
                visitor.setBook(null);
                registeredVisitor.setBook(null);
                System.out.println(Thread.currentThread().getName() + ": Книга возвращена.");
                return true;
            } else {
                System.out.println(Thread.currentThread().getName() + ": Невозможно вернуть книгу.");
                return false;
            }
        }

        // Хранение оценки в мапе ConcurrentHashMap<String, String> scoredBooks: [ ключ: bookName  значение: "средняяОценка/количествоОценок" ] , пример: scoredBooks.put("book1", "3.5/2");
        public double getBookScoreByName(String bookName) {
            if (scoredBooks.containsKey(bookName)) {
                return Double.parseDouble(scoredBooks.get(bookName).split("/")[0]);
            } else {
                return 0.0;
            }
        }

        // Хранение оценки в мапе ConcurrentHashMap<String, String> scoredBooks: [ ключ: bookName  значение: "средняяОценка/количествоОценок" ] , пример: scoredBooks.put("book1", "3.5/2");
        private void setBookScore(Visitor visitor, int score) {
            if (score >= 1 && score <= 10) {
                if (!scoredBooks.containsKey(visitor.getBook().getBookName())) {
                    scoredBooks.put(visitor.getBook().getBookName(), score * 1.0 + "/1");
                } else {
                    String[] s = scoredBooks.get(visitor.getBook().getBookName()).split("/");
                    int countScores = Integer.parseInt(s[1]);
                    scoredBooks.put(visitor.getBook().getBookName(),
                            ((int) (((Double.parseDouble(s[0]) * countScores + score) / (countScores + 1)) * 10)) / 10.0 + "/" + (countScores + 1));
                }
                System.out.println(Thread.currentThread().getName() + ": Книга \"" + visitor.getBook().getBookName() + "\", новая оценка: " + scoredBooks.get(visitor.getBook().getBookName()));
            } else {
                System.out.println("Возврат без оценки.");
                System.out.println("Оценка может быть от 1 до 10 включительно.");
            }
        }

        private boolean isBorrower(String id) {
            return borrowersId.contains(id);
        }

        public Book borrowBook(Visitor visitor, String bookName) {
            //System.out.println("borrowBook");
            Visitor registeredVisitor = idToRegisteredVisitor.get(registerVisitor(visitor));
            //System.out.println(registeredVisitor.getName());
            Book book = findBookByName(bookName);
            //System.out.println(book.getBookName());
            if (!isBorrower(registeredVisitorToId.get(registeredVisitor)) && book != null && !borrowedBooks.contains(book.getBookName())) {
                borrowersId.add(registeredVisitorToId.get(registeredVisitor));
                borrowedBooks.add(book.getBookName());
                visitor.setBook(book);
                registeredVisitor.setBook(book);
                System.out.println(Thread.currentThread().getName() + ": Одолжена книга: \n" + book);
                return book;
            } else {
                System.out.println(Thread.currentThread().getName() + ": Невозможно одолжить книгу.");
                return null;
            }
        }

        public void addVisitorToDeque(Visitor visitor) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
            visitorDeque.add(visitor);
        }

        public int getCurrentDequeSize() {
            return visitorDeque.size();
        }

        public void serve() throws InterruptedException {
            Visitor visitor = visitorDeque.poll();
            assert visitor != null;
            if (visitor.getBook() != null) {     ///////////////  Имитация работы библиотекаря при помощи задержек Thread.sleep() иначе успевает один или  2 - 3
                try {
                    Thread.sleep(new Random().nextInt(150) + 150);
                } catch (InterruptedException e) {
                }
                returnBook(visitor, new Random().nextInt(11)); // Возврат с рандомной оценкой
            } else {
                try {
                    Thread.sleep(new Random().nextInt(150) + 150);
                } catch (InterruptedException e) {
                }
                borrowBook(visitor, getBookArray().get(new Random().nextInt(getBookArray().size())).getBookName());  // Одолжить рандомную книгу
            }
        }


        @Override
        public void run() {
            Thread.currentThread().setName(this.NAME);
            while (isOpen()) {
                while (visitorDeque.isEmpty() && isOpen()) {
                    Thread.yield();
                }
                while (!visitorDeque.isEmpty()) {
                    try {
//                        Thread.sleep(100);
                        serve();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                //System.out.println(Thread.currentThread().getName());
            }
            Thread.currentThread().interrupt();
        }
    }

}

