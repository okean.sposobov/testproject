package dz.dz3_2;

/**
 *  Класс посетителя, поля: name (считается уникальным), id (уникальный персональный идентификатор, назначаемый библиотекай),
 *  book (ссылка на книгу из библиотеке, присваивается при взаимодействии с библиотекой)
 */

public class Visitor {
    private final String name;
    private String id;
    private Book book;

    public Visitor(String name) {
        this.name = name;
        this.id = null;
        this.book = null;
    }

    public Visitor(Visitor visitor) {
        this.name = visitor.getName();
        this.id = null;
        this.book = null;
    }


    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public boolean setIdByLibrary(Library.LibrarianVisitorDequeActions librarian, Visitor registeredVisitor) {
        if (librarian != null && registeredVisitor != null && librarian.isRegisteredVisitor(registeredVisitor) && this.equals(registeredVisitor)) {
            this.id = librarian.getRegisteredVisitorId(registeredVisitor);
            return true;
        } else {
//            System.out.println("id не получен.");
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Visitor testVisitor = (Visitor) obj;
        return this.name.compareTo(testVisitor.getName()) == 0;
    }

}
