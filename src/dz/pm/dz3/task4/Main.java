package dz.pm.dz3.task4;

/**
 * Написать метод, который с помощью рефлексии получит все интерфейсы
 * класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {

        System.out.println("D extends C extends B extends A");
        List<Class<?>> result = getAllInterfaces(D.class);
        System.out.println("_______ All interfaces _______");
        result.forEach((System.out::println));
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        class LocalUtil {
            public static List<Class<?>> getSuperInterfaces(Class<?> intrfaze) {
                Class<?>[] iArr = intrfaze.getInterfaces();
                if (iArr.length == 0) {
                    return new ArrayList<>();
                }
                System.out.println(intrfaze + " extends: \n" + Arrays.toString(iArr));
                List<Class<?>> list = new ArrayList<>();
                for (Class<?> inf : iArr) {
                    list.add(inf);
                    list.addAll(getSuperInterfaces(inf));
                }
                return list;
            }
        }

        List<Class<?>> interfaces = new ArrayList<>();
        Class<?>[] interfacesArr;

        while (cls != Object.class) {
            interfacesArr = cls.getInterfaces();
            System.out.println("class: " + cls.getName() + " implements " + Arrays.toString(interfacesArr));
            for (Class<?> i : interfacesArr) {
                interfaces.add(i);
                interfaces.addAll(LocalUtil.getSuperInterfaces(i));
            }
            System.out.println();
            cls = cls.getSuperclass();
        }

        return new LinkedHashSet<>(interfaces).stream().toList();
    }
}
