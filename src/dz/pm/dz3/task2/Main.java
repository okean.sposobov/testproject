package dz.pm.dz3.task2;

/**
 * Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
 * любом переданном классе и выведет значение, хранящееся в аннотации, на
 * экран.
 */

import dz.pm.dz3.task1.IsLike;
import dz.pm.dz3.task1.MyClass;

import java.lang.annotation.Annotation;

public class Main {
    public static void main(String[] args) {
        checkAnnotationIsLike(MyClass.class);
        checkAnnotationIsLike(NotMyClass.class);
        checkAnnotationIsLike(Annotation.class);
        checkAnnotationIsLike(Main.class);
    }

    public static void checkAnnotationIsLike(Class<?> clazz) {
        int count = 0;
        for (Annotation a : clazz.getAnnotations()) {
            if (a instanceof IsLike) {
                System.out.println(clazz + " имеет аннотацию " + a + ", значение в аннотации: " + clazz.getAnnotation(IsLike.class).like());
//                System.out.println(clazz.getAnnotation(IsLike.class).like());
                count++;
            }
        }

        if (count == 0) {
            System.out.println(clazz + " Не имеет аннотации @IsLike.");
        }
    }
}
