package dz.pm.dz3.task1;

/**
 * Создать аннотацию @IsLike, применимую к классу во время выполнения
 * программы. Аннотация может хранить boolean значение.
 */

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Arrays.stream(MyClass.class.getAnnotations()).forEach(System.out::println);
    }
}
