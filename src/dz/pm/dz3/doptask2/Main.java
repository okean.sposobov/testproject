package dz.pm.dz3.doptask2;

/**
 * Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
 * Необходимо написать метод, принимающий эту строку и выводящий результат,
 * является ли она правильной скобочной последовательностью или нет.
 * Условия для правильной скобочной последовательности те, же что и в задаче 1,
 * но дополнительно:
 * ● Открывающие скобки должны быть закрыты однотипными
 * закрывающими скобками.
 * ● Каждой закрывающей скобке соответствует открывающая скобка того же
 * типа.
 * Пример:
 * Входные данные Выходные данные
 * {()[]()} true
 * {)(} false
 * [} false
 * [{(){}}][()]{} true
 */

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String[] brackets = {
                "{([])(}]", // false
                "({)[}]", // false
                "{)([])}", // false
                "[{([])}{}[(){[]}]()]", // true

                "{()[]()}", // true
                "{)(}", // false
                "[}", // false
                "[{(){}}][()]{}" // true
        };
        for (String s : brackets) {
            System.out.println(s + "  " + validate(s));
        }
    }

    public static boolean validate(String brackets) {
        if (brackets.length() == 0) {
            return true;
        }
        if (brackets.length() % 2 != 0) {
            return false;
        }

        StringBuilder sb = new StringBuilder(brackets);
        Map<Character, Character> map = new HashMap<>();
        map.put(')', '(');
        map.put(']', '[');
        map.put('}', '{');

        for (int i = 0; i < sb.length(); i++) {
            if (map.containsKey(sb.charAt(0)) || map.containsValue(sb.charAt(sb.length() - 1))) {
                return false;
            }
            if (map.containsKey(sb.charAt(i))) {
                if (sb.length() > 1 && sb.charAt(i - 1) == map.get(sb.charAt(i))) {
                    sb.deleteCharAt(i).deleteCharAt(--i);
                    --i;
                } else {
                    return false;
                }
            }
        }

        return sb.length() == 0;
    }
}
