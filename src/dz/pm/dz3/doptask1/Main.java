package dz.pm.dz3.doptask1;

/**
 * Дана строка, состоящая из символов “(“ и “)”
 * Необходимо написать метод, принимающий эту строку и выводящий результат,
 * является ли она правильной скобочной последовательностью или нет.
 * Строка является правильной скобочной последовательностью, если:
 * ● Пустая строка является правильной скобочной последовательностью.
 * ● Пусть S — правильная скобочная последовательность, тогда (S) есть
 * правильная скобочная последовательность.
 * ● Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
 * есть правильная скобочная последовательность.
 * Пример:
 * Входные данные Выходные данные
 * (()()()) true
 * )( false
 * (() false
 * ((())) true
 */

public class Main {
    public static void main(String[] args) {
        String[] strings = {
                "(()()())", // true
                ")(", // false
                "(()", // false
                "((()))" // true
        };
        for (String s : strings) {
            System.out.println(s + " " + validate(s));
        }
        System.out.println();
        String s = "(())()((()()))(())";
        System.out.println(s);
        System.out.println(validate(s) + " validate(s)");
        System.out.println(dz.pm.dz3.doptask2.Main.validate(s)+" dz.pm.dz3.doptask2.Main.validate(s)");
    }

    public static boolean validate(String brackets) {
        if (brackets.length() == 0) {
            return true;
        }
        if (brackets.length() % 2 != 0) {
            return false;
        }

        int count = 0;
        for (char ch : brackets.toCharArray()) {
            if (ch == '(') {
                count++;
            }
            if (ch == ')') {
                count--;
            }
            if (count < 0) {
                return false;
            }
        }

        return count == 0;
    }

}
