package dz.pm.dz3.task3;

/**
 * Есть класс APrinter:
 * public class APrinter {
 *  public void print(int a) {
 *      System.out.println(a);
 *  }
 * }
 * Задача: с помощью рефлексии вызвать метод print() и обработать все
 * возможные ошибки (в качестве аргумента передавать любое подходящее
 * число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Class<APrinter> cls = APrinter.class;
        try {
            Constructor<APrinter> constructor = cls.getConstructor();
            APrinter aPrinter = constructor.newInstance();
            Method methodPrint = aPrinter.getClass().getMethod("print", int.class);
            methodPrint.invoke(aPrinter, 5);

        } catch (NoSuchMethodException e) {
            System.out.println("No such method: " + e.getMessage());
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
