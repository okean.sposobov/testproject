package dz.pm.dz1.task2;

public class MyUncheckedException extends RuntimeException {

    MyUncheckedException() {
        super("MyUncheckedException");
    }

    MyUncheckedException(String message) {
        super(message);
    }
}
