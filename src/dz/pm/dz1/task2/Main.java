package dz.pm.dz1.task2;

/**
 * Создать собственное исключение MyUncheckedException, являющееся
 * непроверяемым.
 */

public class Main {
    public static void main(String[] args) {

        try {
            throw new MyUncheckedException();
        } catch (MyUncheckedException e) {
            System.out.println(e.getMessage());
        }

        methodWithMyUncheckedException();
    }

    public static void methodWithMyUncheckedException() {
        throw new MyUncheckedException("MyUncheckedException from methodWithMyUncheckedException()");
    }
}


