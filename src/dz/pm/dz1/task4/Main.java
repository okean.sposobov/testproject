package dz.pm.dz1.task4;

/**
 * Создать класс MyEvenNumber, который хранит четное число int n. Используя
 * исключения, запретить создание инстанса MyPrimeNumber с нечетным числом.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n % 2 == 0) {
            MyEvenNumber myEvenNumber = new MyEvenNumber(n);
        } else {
            throw new RuntimeException("Нечётное число!");
        }

//        try {
//            MyEvenNumber myEvenNumber = new MyEvenNumber(n);
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            return;
//        }

        MyPrimeNumber myPrimeNumber = new MyPrimeNumber(n);
    }
}
