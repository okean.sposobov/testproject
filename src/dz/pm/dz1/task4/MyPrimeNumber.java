package dz.pm.dz1.task4;

public class MyPrimeNumber {
    private int primeNumber = 1;

    public int getPrimeNumber() {
        return primeNumber;
    }

    MyPrimeNumber(int n){
        if (n % 2 == 0) {
            throw new IllegalArgumentException("Для создания инстанса MyPrimeNumber необходимо нечётное число!");
        }
        primeNumber = n;

    }
}
