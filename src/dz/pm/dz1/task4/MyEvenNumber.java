package dz.pm.dz1.task4;

public class MyEvenNumber {
    private int evenNumber = 0;

    public int getEvenNumber() {
        return evenNumber;
    }

    MyEvenNumber(int n) {
        if (n % 2 != 0) {
            throw new IllegalArgumentException("Для создания инстанса MyEvenNumber необходимо чётное число!");
        }
        evenNumber = n;
    }
}
