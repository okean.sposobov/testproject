package dz.pm.dz1.dop.doptask1;

/**
 *  На вход подается число n и массив целых чисел длины n.
 * Вывести два максимальных числа в этой последовательности.
 * Пояснение: Вторым максимальным числом считается тот, который окажется
 * максимальным после вычеркивания первого максимума.
 */

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        Arrays.sort(a);
        System.out.println(a[a.length - 1] + " " + a[a.length - 2]);
    }
}
