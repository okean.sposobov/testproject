package dz.pm.dz1.dop.doptask2;

/**
 * На вход подается число n, массив целых чисел отсортированных по
 * возрастанию длины n и число p. Необходимо найти индекс элемента массива
 * равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
 * вывести -1.
 * Решить задачу за логарифмическую сложность.
 */

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int p = scanner.nextInt();

        int index = Arrays.binarySearch(a, p);
        System.out.println(index < 0 ? -1 : index);
    }
}
