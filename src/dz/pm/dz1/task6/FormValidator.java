package dz.pm.dz1.task6;

public class FormValidator {
    public static class IncorrectFormatException extends Exception {
        IncorrectFormatException(String message) {
            super(message);
        }
    }

    public static void checkName(String name) throws IncorrectFormatException {
        if (!name.matches("[A-Z][a-z]{1,19}")) {
            throw new IncorrectFormatException("Incorrect name format!");
        }
    }

    public static void checkBirthdate(String birthdate) throws IncorrectFormatException {
        if (!birthdate.matches("[0-3][0-9]\\.\\d{2}\\.\\d{4}")) {
            throw new IncorrectFormatException("Incorrect birthdate format!");
        }
    }

    public static void checkGender(String gender) throws IncorrectFormatException {
        if (!Gender.MALE.toString().equals(gender.toUpperCase())) {
            throw new IncorrectFormatException("Incorrect gender format!");
        }
    }

    public static void checkHeight(String height) throws IncorrectFormatException {
        try {
            if (!(Double.parseDouble(height) > 0)) {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new IncorrectFormatException("Incorrect height format!");
        }
    }
}
