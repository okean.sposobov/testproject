package dz.pm.dz1.task3;

/**
 * Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
 * ./output.txt текст из input, где каждый латинский строчный символ заменен на
 * соответствующий заглавный. Обязательно использование try с ресурсами.
 */

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        String tmpDir = System.getProperty("user.dir") + "/tmpDir";
        File fTmpDir = new File(tmpDir);
        fTmpDir.mkdir();
        String input = tmpDir + "/input.txt";
        String output = tmpDir + "/output.txt";
        File inputFile = new File(input);
        File outputFile = new File(output);

        try (Writer writer = new FileWriter(input)) {
            String s = "a a a A \n B bbb \n";
            writer.write(s);
            System.out.println("Файл input.txt содержит:\n" + s);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try (Scanner scanner = new Scanner(new File(input)); Writer wr = new FileWriter(output)) {
            System.out.println("\nФайл output.txt содержит:");
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine().toUpperCase();
                wr.write(s + "\n");
                System.out.println(s);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        inputFile.delete();
        outputFile.delete();
        fTmpDir.delete();
    }
}
