package dz.pm.dz1.task1;

public class MyCheckedException extends Exception {

    MyCheckedException() {
        super("MyCheckedException");

    }

    MyCheckedException(String message) {
        super(message);
    }
}
