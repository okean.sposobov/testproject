package dz.pm.dz6.doptask2;

/**
 * Напишите программы, чтобы узнать, является ли введенное число простым или нет
 */

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int d = new Scanner(System.in).nextInt();
        BigInteger bI = BigInteger.valueOf(d);
        System.out.println(bI.isProbablePrime((int) Math.log(d)));
    }
}
