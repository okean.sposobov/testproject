package dz.pm.dz6.doptask1;

/**
 * Напишите программу для проверки, является ли введение число - числом Армстронга
 * Число Армстронга — натуральное число, которое в данной системе счисления равно сумме
 * своих цифр, возведенных в степень, равную количеству его цифр. Иногда, чтобы считать
 * число таковым, достаточно, чтобы степени, в которые возводятся цифры, были равны m.
 * Например, десятичное число
 * 153— число Армстронга, потому что
 * 1^3 + 5^3 + 3^3 = 153.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int d = new Scanner(System.in).nextInt();
        String s = Integer.toString(d);
        int p = s.length();
        int dA = 0;
        for (int i = 0; i < s.length(); i++) {
            dA += Math.pow(Integer.parseInt(String.valueOf(s.charAt(i))), p);
        }
        System.out.println(d == dA);
    }
}

