select flowers.flower, orders.number from orders
join flowers on orders.flowers_for_sale_id = flowers."id"
where orders.price = (select max(orders.price) from orders);