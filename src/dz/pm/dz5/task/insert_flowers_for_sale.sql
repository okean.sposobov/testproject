insert into flowers_for_sale(flower_id, current_price, for_sale) values
((select id from flowers where flower = 'Роза'), 100, true );

insert into flowers_for_sale(flower_id, current_price, for_sale) values
((select id from flowers where flower = 'Лилия'), 50, true );

insert into flowers_for_sale(flower_id, current_price, for_sale) values
((select id from flowers where flower = 'Ромашка'), 25, true );

insert into flowers_for_sale(flower_id, current_price, for_sale, description) values
((select id from flowers where flower = 'Кактус'), 333, true, 'Нарасхват!!!' );

insert into flowers_for_sale(flower_id, current_price, for_sale, description) values
((select id from flowers where flower = 'Пион'), 30, false , 'Не сезон' );