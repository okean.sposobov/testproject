select
orders."id" ,
orders."number" ,
orders.price as order_price ,
flowers_for_sale.current_price as flower_price,
orders.date_time ,
clients.client_name ,
clients.description as client_description ,
flowers.flower ,
flowers.description as flower_description
from orders
join clients on orders.client_id = clients.id
join flowers_for_sale on flowers_for_sale_id = flowers_for_sale.id
join flowers on flower_id = flowers."id"
where orders.id = 1;