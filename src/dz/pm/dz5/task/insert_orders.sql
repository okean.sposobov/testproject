insert into orders (
client_id,
 flowers_for_sale_id,
  number,
   price
   )
values (
1,
1,
900,
900 * 100
),
(
3,
3,
3,
3 * 25
);

insert into orders (
client_id,
 flowers_for_sale_id,
  number,
   price,
   date_time
   )
values (
1,
1,
500,
500 * 100,
now()-'6 week'::interval
),
(
3,
3,
7,
7 * 25,
now()-'6 week'::interval
)