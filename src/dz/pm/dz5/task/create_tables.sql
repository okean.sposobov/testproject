create table if not exists flowers
(
    id serial primary key,
    flower varchar(50) not null,
    description text
);

create table if not exists flowers_for_sale
(
    id serial primary key,
    flower_id integer references flowers(id),
    for_sale boolean not null,
    current_price money not null ,
    description text
);

create table if not exists clients
(
    id serial primary key,
    client_name varchar(50) not null,
    phone varchar(15) not null,
    description text
);

create table if not exists orders
(
    id serial primary key,
    client_id integer references clients(id) not null ,
    flowers_for_sale_id integer references flowers_for_sale(id) not null ,
    number smallint constraint max_number check (number > 0 and number <= 1000),
    price numeric not null ,
    date_time timestamp default current_timestamp ,
    description text
);