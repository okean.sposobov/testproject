package dz.pm.dz4.task1;

/**
 * Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
 * экран.
 */

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        System.out.println(Stream
                .iterate(2, (integer) -> integer + 2)
                .limit(50)
                .reduce(Integer::sum)
                .get());
    }
}
