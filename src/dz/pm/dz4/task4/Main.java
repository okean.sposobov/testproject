package dz.pm.dz4.task4;

/**
 * На вход подается список вещественных чисел. Необходимо отсортировать их по
 * убыванию.
 */

import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(-1.0, 3.0, 7.0, 7.0, -4.0, 8.0);
        list.stream()
                .sorted(Comparator.reverseOrder())
//                .sorted((a, b) -> -Double.compare(a, b))
//                .sorted((a, b) -> a >= b ? -1 : 1)
                .forEach(System.out::println);
    }
}
