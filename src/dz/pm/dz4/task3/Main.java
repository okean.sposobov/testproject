package dz.pm.dz4.task3;

/**
 * На вход подается список строк. Необходимо вывести количество непустых строк в
 * списке.
 * Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        System.out.println(list.stream()
                .filter(s -> s.length() > 0)
                .count());
    }
}
