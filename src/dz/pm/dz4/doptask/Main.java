package dz.pm.dz4.doptask;

/**
 * На вход подается две строки.
 * Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
 * возможных операций:
 * 1. Добавить символ
 * 2. Удалить символ
 * 3. Заменить символ
 * Пример:
 * “cat” “cats” -> true
 * “cat” “cut” -> true
 * “cat” “nut” -> false
 */

public class Main {
    public static void main(String[] args) {
        String[][] strings = {
                {"cat", "cats"}, // true
                {"cat", "cut"},  // true
                {"cat", "nut"},  // false
                {"cat", "cuts"}, // false
                {"cat", "catss"} // false
        };

        for (String[] string : strings) {
            System.out.println(checkStrings(string[1], string[0]));
        }
    }

    public static boolean checkStrings(String s1, String s2) {
        int lengthDifference = Math.abs(s1.length() - s2.length());
        if (lengthDifference == 1) {
            return s1.contains(s2) || s2.contains(s1);
        }

        if (lengthDifference == 0) {
            char[] chArr1 = s1.toCharArray();
            char[] chArr2 = s2.toCharArray();
            int count = 0;
            for (int i = 0; i < chArr1.length; i++) {
                if (chArr1[i] != chArr2[i]) {
                    count++;
                }
            }
            return count < 2;
        }

        return false;
    }
}
