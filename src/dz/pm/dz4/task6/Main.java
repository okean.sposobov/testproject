package dz.pm.dz4.task6;

/**
 * Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setOfSets = Set.of(
                Set.of(1, 2, 3),
                Set.of(3, 4, 5),
                Set.of(5, 6, 7)
        );
        System.out.println(
                setOfSets.stream()
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet())
        );

//        Set<Integer> set = new HashSet<>();
//        setOfSets.forEach(set::addAll);
//        System.out.println(set);
    }
}
