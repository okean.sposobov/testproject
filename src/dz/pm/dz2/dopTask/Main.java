package dz.pm.dz2.dopTask;

/**
 * Реализовать метод, который принимает массив words и целое положительное число k.
 * Необходимо вернуть k наиболее часто встречающихся слов..
 * Результирующий массив должен быть отсортирован по убыванию частоты
 * встречаемого слова. В случае одинакового количества частоты для слов, то
 * отсортировать и выводить их по убыванию в лексикографическом порядке.
 * Пример:
 * Входные данные
 * words =
 * ["the","day","is","sunny","the","the","the",
 * "sunny","is","is","day"]
 * k = 4
 * Выходные данные
 * ["the","is","sunny","day"
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] words = {"the", "b", "day", "b", "is", "b", "sunny", "b", "the", "b", "the", "b", "the", "c", "sunny", "c", "is", "d", "is", "d", "day", "a"};
        System.out.println(Arrays.toString(words));
        System.out.println(Arrays.toString(wordsCount(words, 4)));
    }

    public static String[] wordsCount(String[] words, int k) {
        if (words.length < 1) {
            System.out.println("Don't!");
            return new String[0];
        }

        List<String> list = Arrays.asList(words);

        if (new HashSet<String>(list).size() < k || k <= 0) {
            System.out.println("Нельзя так! k должно быть < или = количеству уникальных слов и > 0.");
            return new String[0];
        }

        Map<String, Integer> map = new HashMap<>();
        list.forEach((word) -> {
            if (map.containsKey(word)) {
                map.compute(word, (String key, Integer oldValue) -> oldValue + 1);
            } else {
                map.put(word, 1);
            }
        });
        System.out.println(map);

        List<Map.Entry<String, Integer>> listEntry = new ArrayList<>(map.entrySet());
        listEntry.sort((o1, o2) -> {
            if (o1.getValue() > o2.getValue()) return -1;
            if (o1.getValue() < o2.getValue()) return 1;
            return o1.getKey().compareTo(o2.getKey()) > 0 ? -1 : 1;
        });
        System.out.println(listEntry);

        List<String> resultList = new ArrayList<>();
        listEntry.subList(0, k).forEach((entry -> resultList.add(entry.getKey())));

        return resultList.toArray(new String[0]);
    }
}
