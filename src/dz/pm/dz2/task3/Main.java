package dz.pm.dz2.task3;

/**
 * Реализовать класс PowerfulSet, в котором должны быть следующие методы:
 * a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
 * пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
 * Вернуть {1, 2}
 * b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
 * объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
 * Вернуть {0, 1, 2, 3, 4}
 * c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
 * возвращает элементы первого набора без тех, которые находятся также
 * и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        PowerfulSet powerfulSet = new PowerfulSet();

        Integer[] a1 = {1, 2, 3};
        Integer[] a2 = {0, 1, 2, 4};


        Set<Integer> set1 = new HashSet<>(Arrays.asList(a1));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(a2));

        System.out.println("set1: " + set1);
        System.out.println("set2: " + set2);
        System.out.println();
        System.out.println("intersection: " + powerfulSet.intersection(set1, set2));
        System.out.println("union: " + powerfulSet.union(set1, set2));
        System.out.println("relativeComplement: " + powerfulSet.relativeComplement(set1, set2));
        System.out.println();
        System.out.println("set1: " + set1);
        System.out.println("set2: " + set2);
    }
}
