package dz.pm.dz2.task1;

/**
 * Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
 * набор уникальных элементов этого массива. Решить используя коллекции.
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(1);
        list.add(2);

        System.out.println(arrayListToSet(list));

    }

    public static <T> Set<T> arrayListToSet(List<T> arrayList) {
        return new HashSet<>(arrayList);
    }
}
