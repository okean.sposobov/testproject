package dz.pm.dz2.task2;

/**
 * С консоли на вход подается две строки s и t. Необходимо вывести true, если
 * одна строка является валидной анаграммой другой строки и false иначе.
 * Анаграмма — это слово или фраза, образованная путем перестановки букв
 * другого слова или фразы, обычно с использованием всех исходных букв ровно
 * один раз.
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        String t = scanner.next();

        ArrayList<Character> aS = new ArrayList<>();
        ArrayList<Character> aT = new ArrayList<>();

        for (Character ch: s.toCharArray()){
            aS.add(ch);
        }

        for (Character ch: t.toCharArray()){
            aT.add(ch);
        }

        Collections.sort(aS);
        Collections.sort(aT);

        System.out.println(aS.equals(aT));
    }
}
