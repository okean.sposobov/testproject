package dz.pm.dz2.task4;

/**
 * В некоторой организации хранятся документы (см. класс Document). Сейчас все
 * документы лежат в ArrayList, из-за чего поиск по id документа выполняется
 * неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
 * перевести хранение документов из ArrayList в HashMap.
 * public class Document {
 * public int id;
 * public String name;
 * public int pageCount;
 * }
 * Реализовать метод со следующей сигнатурой:
 * public Map<Integer, Document> organizeDocuments(List<Document> documents)
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        List<Document> list = generateDocuments(5);
        Map<Integer, Document> map = new DocumentOrganizer().organizeDocuments(list);
        map.forEach(((id, document) -> System.out.println("document id: " + id + "\n" + documentToString(document))));
    }

    public static List<Document> generateDocuments(int number) {
        List<Document> list = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            Document document = new Document();
            document.id = i;
            document.name = "name" + i;
            document.pageCount = 3;
            list.add(document);
        }
        return list;
    }

    public static String documentToString(Document document) {
        return "id: " + document.id + "\n" +
                "name: " + document.name + "\n" +
                "pageCount: " + document.pageCount + "\n";
    }
}
