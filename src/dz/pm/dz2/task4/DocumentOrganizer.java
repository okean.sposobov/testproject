package dz.pm.dz2.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentOrganizer {
    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        documents.forEach((document)-> map.put(document.id, document));
        return map;
    }

}
