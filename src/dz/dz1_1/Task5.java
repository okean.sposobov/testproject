package dz.dz1_1;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        final double SM_PER_INCH = 2.54;
        Scanner scanner = new Scanner(System.in);
        int inches = scanner.nextInt();
        System.out.println(SM_PER_INCH * inches);


    }
}
