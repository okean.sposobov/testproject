package dz.dz1_1;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        final int SECONDS_PER_MINUTE = 60;
        final int MINUTES_PER_HOUR = 60;

        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();
        int totalMinutes, minutes, hours;


        totalMinutes = s / SECONDS_PER_MINUTE;
        hours = totalMinutes / MINUTES_PER_HOUR;
        minutes = totalMinutes % MINUTES_PER_HOUR;

        System.out.println(hours + " " + minutes);

    }
}
