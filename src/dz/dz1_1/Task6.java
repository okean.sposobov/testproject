package dz.dz1_1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        final double KM_PER_MILE = 1.60934;
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        System.out.println(count / KM_PER_MILE);

    }
}
