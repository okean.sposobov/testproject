package dz.dz2_1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        double[] d = new double[N];
        for (int i = 0; i < d.length; i++) {
            d[i] = scanner.nextDouble();
        }
        System.out.println(mid(d));

    }

    static double mid(double[] a) {
        double sum = 0;
        for (double v : a) {
            sum += v;
        }
        return sum/a.length;
    }
}
