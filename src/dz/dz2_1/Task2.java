package dz.dz2_1;

import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < N; i++) {
            a[i] = scanner.nextInt();
        }

        int M = scanner.nextInt();
        int[] b = new int[M];
        for (int i = 0; i < M; i++) {
            b[i] = scanner.nextInt();
        }


        if (N!=M){
            System.out.println(false);
        } else {
            System.out.println(arrayEqual(a,b));
        }

    }

    static int[] arrayInit() {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        return a;
    }

    static boolean arrayEqual(int[] a, int[] b) {
        if (a.length == b.length) {
            for (int i = 0; i < a.length; i++) {
                if (a[i] != b[i]) return false;
            }
            return true;
        } else return false;

    }


}
//        int[] a = arrayInit();
//        int[] b = arrayInit();
//        System.out.println(arrayEqual(a, b));
//    }
//
//    static int[] arrayInit() {
//        Scanner scanner = new Scanner(System.in);
//        int N = scanner.nextInt();
//        int[] a = new int[N];
//        for (int i = 0; i < a.length; i++) {
//            a[i] = scanner.nextInt();
//        }
//        return a;
//    }
//
//    static boolean arrayEqual(int[] a, int[] b) {
//        if (a.length == b.length) {
//            for (int i = 0; i < a.length; i++) {
//                if (a[i] != b[i]) return false;
//            }
//            return true;
//        } else return false;
//
//    }
//}
