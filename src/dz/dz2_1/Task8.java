package dz.dz2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);   //  -10 9 -5 -6 1 -3
        int N = scanner.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        int M = scanner.nextInt();

        Arrays.sort(a);
        int pos = Arrays.binarySearch(a, M);
        if (pos < 0) {
            if (pos == -1) {
                System.out.println(a[0]);
                return;
            }
            if (pos == -N - 1) {
                System.out.println(a[N - 1]);
                return;
            }
            pos = Math.abs(pos);
            if (Math.abs(a[pos - 1] - M) <= Math.abs(a[pos - 2] - M)) {
                System.out.println(a[pos - 1]);
            } else {
                System.out.println(a[pos - 2]);
            }
        } else {
            System.out.println(a[pos]);
        }
    }
}
