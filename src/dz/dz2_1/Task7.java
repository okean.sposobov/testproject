package dz.dz2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] a = new int[N];

        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
            a[i] = a[i] * a[i];
        }

        String s = "";
        Arrays.sort(a);
        for (int e : a) {
            s += e + " ";
        }

        System.out.println(s.trim());
    }
}
