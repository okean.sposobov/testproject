package dz.dz2_1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();

        String[] morze = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--"
                , "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-"
                , "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        String s = "";
        for (int i = 0; i < word.length(); i++) {
            s += morze[(int) word.charAt(i) - 1040] + " ";
            //System.out.print( morze[(int)word.charAt(i)-1040] + " " );
        }

        System.out.println(s.trim());
    }
}
