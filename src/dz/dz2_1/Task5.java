package dz.dz2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();                  // 38 44 0 -11 2
        int[] a = new int[N];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        int M = scanner.nextInt();
        M = M % N;

        //////////////////////////////////////
//        int lastPos = a.length - 1;
//        for (int m = 0; m < M; m++) {
//            int tmp = a[lastPos];
//            for (int i = lastPos; i > 0; i--) {
//                a[i] = a[i - 1];5
//            }
//            a[0] = tmp;
//        }
        /////////////////////////////////////////

        int[] b = new int[N];
        System.arraycopy(a, N - M, b, 0, M);
        System.arraycopy(a, 0, b, M, N - M);
        a = b;

        String s = "";
        for (int i = 0; i < N; i++) {
            s += a[i] + " ";
            //System.out.print(a[i] + " ");
        }
        System.out.println(s.trim());

    }
}
