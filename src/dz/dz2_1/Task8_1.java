package dz.dz2_1;

import java.util.Scanner;

public class Task8_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);   //  -10 9 -5 -6 1 -3
        int N = scanner.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        int M = scanner.nextInt();

        int dif = Math.abs(a[0] - M);
        int index = 0;
        for (int i = 1; i < N; i++) {
            if (Math.abs(a[i] - M) <= dif) {
                dif = Math.abs(a[i] - M);
                index = i;
            }
        }

    }
}
