package dz.dz2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        int X = scanner.nextInt();
        int pos = Arrays.binarySearch(a, X);

        if (pos < 0) {
            if (pos == -1) {
                System.out.println(-1);
                return;
            }
            if (pos == -N - 1) {
                System.out.println(N);
                return;
            }

            System.out.println(Math.abs(pos) - 1);
        } else {
            while (pos < N && a[pos] == X ) {
                pos++;
            }
            System.out.println(pos);
        }
    }
}

