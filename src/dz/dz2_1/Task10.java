package dz.dz2_1;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int max = 1001;
        int m = (int) (Math.random() * max);
        boolean stop = false;
        while (!stop) {
            int input = scanner.nextInt();
            if (input < 0) {
                break;
            }

            switch (play(m, input)) {
                case 0 -> {
                    System.out.println("Победа!");
                    stop = true;
                }
                case -1 -> {
                    System.out.println("Это число меньше загаданного.");
                }
                case 1 -> {
                    System.out.println("Это число больше загаданного.");
                }
            }
        }
    }

    static int play(int m, int input) {
        if (input == m) {
            return 0;
        }
        if (input < m) {
            return -1;
        } else return 1;
    }
}
