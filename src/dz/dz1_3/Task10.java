package dz.dz1_3;

import java.util.Arrays;
import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        char[] c = new char[2 * N - 1];
        Arrays.fill(c, ' ');

        for (int i = 0; i < N; i++) {
            Arrays.fill(c, N - i - 1, N + i, '#');
            System.out.println(new String(c).substring(0, N + i));
        }

        for (int i = 0; i < N - 1; i++) System.out.print(" ");
        System.out.print("|");
    }
}
