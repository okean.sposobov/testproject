package dz.dz1_3;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;

        while (true) {
            int d = scanner.nextInt();
            if (d < 0) {
                count++;
            } else break;
        }
        System.out.println(count);
    }
}
