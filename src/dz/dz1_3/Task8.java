package dz.dz1_3;

import java.util.Random;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int sum = 0;
        int d = 0;

        for (int i = 0; i < n; i++) {
            d = scanner.nextInt();
            if (d > p) sum += d;
        }

        System.out.println(sum);

    }
}
