package dz.dz1_3;

import java.util.Arrays;
import java.util.Scanner;

public class Task10_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        char[] c = new char[2 * N - 1];
        Arrays.fill(c, ' ');

        for (int i = 0; i < N; i++) {
            Arrays.fill(c, N - i - 1, N + i, '#');
            System.out.println(c);
        }
        Arrays.fill(c, ' ');
        c[N - 1] = '|';
        System.out.println(c);
    }
}
