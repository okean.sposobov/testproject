package dz.dz3_3.task4;

import java.util.Scanner;

/**
 * Переписать задачу 7 домашнего задания 2 часть 2 в стиле ООП.
 * ● Нужно как минимум завести классы Dog и Participant.
 * ● Нужно реализовать метод, определяющий трех победителей согласно
 * условию.
 * ● Можно добавлять любые дополнительные методы и классы.
 * Условие задачи:
 * Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
 * хранения участников и оценок неудобная, а победителя определить надо. В
 * первой таблице в системе хранятся имена хозяев, во второй - клички животных,
 * в третьей — оценки трех судей за выступление каждой собаки. Таблицы
 * связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
 * строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
 * помочь Пете определить топ 3 победителей конкурса.
 * На вход подается число N — количество участников конкурса. Затем в N
 * строках переданы имена хозяев. После этого в N строках переданы клички
 * собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
 * оценки судей. Победителями являются три участника, набравшие
 * максимальное среднее арифметическое по оценкам 3 судей. Необходимо
 * вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
 * Гарантируется, что среднее арифметическое для всех участников будет
 * различным.
 * Ограничения:
 * 0 < N < 100
 *
 * Входные данные
 *

 4
 Иван
 Николай
 Анна
 Дарья
 Жучка
 Кнопка
 Цезарь
 Добряш
 7 6 7
 8 8 7
 4 5 6
 9 9 9

 * Выходные данные:

 Дарья: Добряш, 9.0
 Николай: Кнопка, 7.6
 Иван: Жучка, 6.6
 */

public class Main {

    static void getWinners(int n, double[][] scores, Participant[] participants, Dog[] dogs) {

        double[] scoresAverage = new double[n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < 3; k++) {
                scoresAverage[i] += scores[i][k] / 3;
            }
        }

        int index = 0;
        for (int i = 0; i < 3; i++) {
            double max = scoresAverage[i];
            for (int k = 0; k < n; k++) {
                if (scoresAverage[k] >= max) {
                    max = scoresAverage[k];
                    index = k;
                }
            }
            System.out.println(participants[index] + ": " + participants[index].dog + ", " + ((int) (scoresAverage[index] * 10)) / 10.0);
            scoresAverage[index] = -1;
        }

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double[][] scores = new double[n][3];
        Participant[] participants = new Participant[n];
        Dog[] dogs = new Dog[n];


        for (int i = 0; i < n; i++) {
            participants[i] = new Participant(scanner.next());
        }

        for (int i = 0; i < n; i++) {
            participants[i].dog = new Dog(scanner.next());
        }

        for (int i = 0; i < n; i++) {
            for (int h = 0; h < 3; h++) {
                scores[i][h] = scanner.nextDouble();
            }
        }

        getWinners(n, scores, participants, dogs);

    }
}

class Dog {
    String name;

    public Dog(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

class Participant {

    Dog dog;
    String name;

    public Participant(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }
}