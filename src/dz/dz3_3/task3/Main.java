package dz.dz3_3.task3;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *  На вход передается N — количество столбцов в двумерном массиве и M —
 * количество строк. Необходимо вывести матрицу на экран, каждый элемент
 * которого состоит из суммы индекса столбца и строки этого же элемента. Решить
 * необходимо используя ArrayList.
 * Ограничения:
 * ● 0 < N < 100
 * ● 0 < M < 100
 *
 * Входные данные

 2 2

 * Выходные данные

 0 1
 1 2

 * Входные данные

 3 5

 * Выходные данные

 0 1 2
 1 2 3
 2 3 4
 3 4 5
 4 5 6

 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            matrix.add(new ArrayList<>());
            for (int k = 0; k < n; k++) {
                matrix.get(i).add(i + k);
                System.out.print(matrix.get(i).get(k) + " ");
            }
            System.out.println();
        }
    }

}
