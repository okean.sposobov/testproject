package dz.dz3_3.task2;

/**
 * Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
 * сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
 * нет. Реализовать метод в цеху, позволяющий по переданной мебели
 * определять, сможет ли ей починить или нет. Возвращать результат типа
 * boolean. Протестировать метод.
 */


public class Main {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        System.out.println(bestCarpenterEver.isFixable(new Stool()));
        System.out.println(bestCarpenterEver.isFixable(new Table()));
    }
}
