package dz.dz3_3.task1;

public class Dolphin extends Animal implements WayOfBirth, Swimming {

    @Override
    public void birth() {
        System.out.println("Дельфин рожает как млекопитающее");
    }

    @Override
    public void swim() {
        System.out.println("Дельфин плывёт быстро");
    }

}
