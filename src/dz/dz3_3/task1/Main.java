package dz.dz3_3.task1;

/**
 * Имеется следующий набор утверждений.
 *  Рассматриваются следующие животные:
 * ● летучая мышь (Bat)
 * ● дельфин (Dolphin)
 * ● золотая рыбка (GoldFish)
 * ● орел (Eagle)
 * Все животные одинаково едят и спят (предположим), и никто из животных не
 * должен иметь возможности делать это иначе.
 * Еще животные умеют по-разному рождаться (wayOfBirth):
 * ● Млекопитающие (Mammal) живородящие.
 * ● Рыбы (Fish) мечут икру.
 * ● Птицы (Bird) откладывают яйца.
 * Помимо этого бывают некоторые особенности, касающиеся передвижения.
 * Бывают летающие животные (Flying) и плавающие (Swimming). Однако орел
 * летает быстро, а летучая мышь медленно. Дельфин плавает быстро, а золотая
 * рыбка медленно.
 * Согласно этим утверждениям нужно создать иерархию, состоящую из классов,
 * абстрактных классов и/или интерфейсов. Каждое действие или утверждение
 * подразумевает под собой вызов void метода, в котором реализован вывод на
 * экран описания текущего действия.
 */


public class Main {
    public static void main(String[] args) {
        Bat bat = new Bat();
        bat.birth();
        bat.eat();
        bat.sleep();
        bat.fly();
        System.out.println();
        Flying flying = new Bat();
        flying.fly();
        WayOfBirth wayOfBirth = new Bat();
        wayOfBirth.birth();
        Animal animal = new Bat();
        ((Bat) animal).fly();
        ((Bat)wayOfBirth).eat();
        System.out.println();

        Eagle eagle = new Eagle();
        eagle.birth();
        eagle.eat();
        eagle.sleep();
        eagle.fly();
        System.out.println();

        Dolphin dolphin = new Dolphin();
        dolphin.birth();
        dolphin.eat();
        dolphin.sleep();
        dolphin.swim();
        System.out.println();

        GoldFish goldFish = new GoldFish();
        goldFish.birth();
        goldFish.eat();
        goldFish.sleep();
        goldFish.swim();


    }
}
