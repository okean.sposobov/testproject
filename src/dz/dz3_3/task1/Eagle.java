package dz.dz3_3.task1;

public class Eagle extends Animal implements Flying, WayOfBirth{

    @Override
    public void birth() {
        System.out.println("Орёл откладывает яйца");
    }

    @Override
    public void fly() {
        System.out.println("Орёл летит быстро");
    }


}
