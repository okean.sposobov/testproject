package dz.dz3_3.task1;

public class Bat extends Animal implements WayOfBirth, Flying {

    @Override
    public void birth() {
        System.out.println("Летучая мышь рожает как млекопитающее");
    }

    @Override
    public void fly() {
        System.out.println("Летучая мышь летит медленно");
    }
}
