package dz.dz3_3.task1;

public class GoldFish extends Animal implements WayOfBirth, Swimming {
    @Override
    public void birth() {
        System.out.println("Золотая рыбка мечет икру");
    }

    @Override
    public void swim() {
        System.out.println("Золотая рыбка плывёт медленно");
    }

}
