package dz.dz2_2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int M = scanner.nextInt();
        int[][] a = new int[M][N];
        int[] mins = new int[M];

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                a[i][j] = scanner.nextInt();
            }
            mins[i] = a[i][0];
            for (int j = 1; j < N; j++) {
                if (mins[i] > a[i][j]) {
                    mins[i] = a[i][j];
                }
            }
        }
        for (int e : mins) {
            System.out.print(e + " ");
        }
    }
}
