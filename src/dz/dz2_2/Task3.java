package dz.dz2_2;

import java.util.Scanner;

import static java.lang.Math.PI;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        char[][] c = new char[n][n];
        int xK = scanner.nextInt();
        int yK = scanner.nextInt();

        int[][] km = new int[8][2];
        int x1 = 2, y1 = 1;
        int x2 = 1, y2 = 2;
        double PIdiv4 = PI / 4, PIdiv2 = PI / 2;
        for (int i = 0; i < 4; i++) {
            int turnX = (int) (Math.sin(PIdiv4 + PIdiv2 * i) * 2);
            int turnY = (int) (Math.cos(PIdiv4 + PIdiv2 * i) * 2);
            km[i][0] = turnX * x1;
            km[i][1] = turnY * y1;
            km[i + 4][0] = turnX * x2;
            km[i + 4][1] = turnY * y2;
        }

        for (int i = 0; i < 4; i++) {
            km[i][0] += xK;
            km[i][1] += yK;
            km[i + 4][0] += xK;
            km[i + 4][1] += yK;
        }

        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c[0].length; j++) {
                c[i][j] = '0';
            }
        }

        c[yK][xK] = 'K';

        for (int i = 0; i < 4; i++) {
            if ((km[i][0] >= 0 && km[i][0] < n) && (km[i][1] >= 0 && km[i][1] < n)) {
                c[km[i][1]][km[i][0]] = 'X';
            }
            if ((km[i + 4][0] >= 0 && km[i + 4][0] < n) && (km[i + 4][1] >= 0 && km[i + 4][1] < n)) {
                c[km[i + 4][1]][km[i + 4][0]] = 'X';
            }
        }

        String s = "";
        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c[0].length; j++) {
                s += c[i][j] + " ";
            }
            System.out.println(s.trim());
            s = "";
        }
    }
}
