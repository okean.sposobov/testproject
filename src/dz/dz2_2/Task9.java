package dz.dz2_2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(digitsBack(n).trim());
    }

    public static String digitsBack(int n) {
        return n / 10 == 0 ? n % 10 + "" : (digitsBack(n / 10) + " " + n % 10);
    }

}
