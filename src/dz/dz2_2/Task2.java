package dz.dz2_2;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[][] a = new int[N][N];
        int X1 = scanner.nextInt();
        int Y1 = scanner.nextInt();
        int X2 = scanner.nextInt();
        int Y2 = scanner.nextInt();

        Arrays.fill(a[Y1], X1, X2 + 1, 1);
        Arrays.fill(a[Y2], X1, X2 + 1, 1);
        for (int i = Y1 + 1; i < Y2; i++) {
            a[i][X1] = a[i][X2] = 1;
        }

        String s = "";
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                s += a[i][j] + " ";
            }
            System.out.println(s.trim());
            s = "";
        }
    }
}
