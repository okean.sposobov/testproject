package dz.dz2_2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                a[i][k] = scanner.nextInt();
            }
        }

        int p = scanner.nextInt();

        int ip = 0, kp = 0;
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                if (a[i][k] == p) {
                    ip = i;
                    kp = k;
                    i = n;  // выход из i цикла по i < n
                    break;
                }
            }
        }

        String s = "";
        for (int i = 0; i < n; i++) {
            if (i == ip) continue;
            for (int k = 0; k < n; k++) {
                if (k == kp) continue;
                s +=a[i][k]+" ";
            }
            System.out.println(s.trim());
            s = "";
        }
    }
}
