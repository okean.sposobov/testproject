package dz.dz2_2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                a[i][k] = scanner.nextInt();
            }
        }

        boolean sim = true;
        for (int i = 0; i < n - 1; i++) {
            for (int k = 0; k < n - 1 - i; k++) {
                if (a[i][k] != a[n - 1 - k][n - 1 - i]) {
                    sim = false;
                    k = n;
                    i = n;
                }
            }
        }
        System.out.println(sim);
    }
}
