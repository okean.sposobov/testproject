package dz.dz2_2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(sum(n));
    }

    public static int sum(int n) {
        return n / 10 == 0 ? n % 10 : (n % 10 + sum(n / 10));
    }

}
