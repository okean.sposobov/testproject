package dz.dz1_dop;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String mailPackage = scanner.nextLine();

        switch (((mailPackage.contains("камни!") ? 1 : 0) + (mailPackage.contains("запрещенная продукция") ? 2 : 0))) {
            case 1 -> System.out.println("камни в посылке");
            case 2 -> System.out.println("в посылке запрещенная продукция");
            case 3 -> System.out.println("в посылке камни и запрещенная продукция");
            default -> System.out.println("все ок");
        }
    }

}
