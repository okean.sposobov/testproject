package dz.dz1_dop;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String model;
        int price;

        model = scanner.nextLine();
        price = scanner.nextInt();

        System.out.println((model.contains("samsung") || model.contains("iphone")) && (price >= 50000 && price <= 120000) ? "Можно купить" : "Не подходит");
    }
}
