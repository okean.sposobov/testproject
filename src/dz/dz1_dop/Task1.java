package dz.dz1_dop;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();

        System.out.println(
                word.matches("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])((?=.*[-_*])(?!.*[\\!\\\"\\#\\$\\%\\&\\'\\(\\)\\+\\,\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\`\\{\\|\\}\\~&&[^-*]])).{8,})") ?
                        "пароль надежный" : "пароль не прошел проверку");


    }
}

// ((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_])(?!.*[!&]).{8,})
//   ((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])((?=.*[-_*])).{8,})

//   HgF_h1j*--*jnh
//   HgF_hj*--*jnh
//   HgF_hj*--*!jnh
//    Hello_22 пароль надежный
//    world234 пароль не прошел проверку
// -*hEl_**lo_22
// !&Hello22_*
//   1worlDERT_*--
//  HgF_h1234j*--*!jnh
//   HgF_h1234j*--*jnh