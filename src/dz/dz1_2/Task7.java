package dz.dz1_2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string;
        int spacePosition;

        string = scanner.nextLine();
        spacePosition = string.indexOf(" ");

        System.out.println(string.substring(0, spacePosition) + "\n" + string.substring(spacePosition + 1));

    }
}
