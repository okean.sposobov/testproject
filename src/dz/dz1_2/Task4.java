package dz.dz1_2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;

        n = scanner.nextInt();

        System.out.println(n >= 6 ? "Ура, выходные!" : 6 - n);
    }
}
