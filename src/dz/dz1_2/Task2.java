package dz.dz1_2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x, y;

        x = scanner.nextInt();
        y = scanner.nextInt();

        System.out.println(x > 0 && y > 0);

    }
}
