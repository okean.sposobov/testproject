package dz.dz1_2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double n;

        n = scanner.nextDouble();

        System.out.println(Math.abs(Math.log(Math.pow(Math.E, n))) - Math.abs(n) <= 1e-11);


    }

}
