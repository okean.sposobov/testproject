package dz.dz1_2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count;

        count = scanner.nextInt();

        System.out.println(
                count < 500 ? "beginner" :
                        count < 1500 ? "pre-intermediate" :
                                count < 2500 ? "intermediate" :
                                        count < 3500 ? "upper-intermediate" :
                                                "fluent"
        );


    }
}
