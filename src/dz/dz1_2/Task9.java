package dz.dz1_2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x;

        x = scanner.nextDouble();

        System.out.println(Math.round((Math.pow(Math.sin(Math.toRadians(x)), 2) + Math.pow(Math.cos(Math.toRadians(x)), 2)) - 1) == 0);


    }
}
