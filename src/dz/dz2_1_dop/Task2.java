package dz.dz2_1_dop;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Велосипед. 1. Сортировка конечного 2. Сортировка из исходного 3. Counting Sort исходного
 */

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
//////////////////////////   1. Сортировка конечного
        int midId = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] >= 0) {
                midId = i;
                break;
            }
        }

        int[] q = new int[n];
        for (int i = 0; i < n; i++) {
            q[i] = a[i] * a[i];
        }
//        System.out.println(Arrays.toString(q));

        int[] resq = new int[n];
        int s = 0, c = 0;
        for (int i = midId - 1; i >= 0; i--) {
            if (midId + c == n) {
                resq[s++] = q[i];
            }
            for (int k = midId + c; k < n; k++) {
                if (q[i] > q[k]) {
                    resq[s++] = q[k];
                    c++;
                }
                if (q[i] == q[k]) {
                    resq[s++] = q[i];
                    resq[s++] = q[i];
                    c++;
                    if (i == 0) {
                        for (int l = k + 1; l < n; l++) {
                            resq[s++] = q[l];
                        }
                        break;
                    }
                    break;
                }
                if (q[i] < q[k]) {
                    resq[s++] = q[i];
                    if (i == 0) {
                        for (int l = k; l < n; l++) {
                            resq[s++] = q[l];
                        }
                        break;
                    }
                    break;
                }
                if (c == n - midId) {
                    resq[s++] = q[i];
                }
            }

        }

        for (int i : resq) {
            System.out.print(i + " ");
        }

///////////////////////////  2.  Сортировка из исходного
//
//        int countN = 0, countP = 0;
//        int[] positive = new int[n];
//        int[] negative = new int[n];
//        for (int i = 0; i < n; i++) {
//            if (a[i] < 0) {
//                countN++;
//                negative[n - 1 - i] = Math.abs(a[i]);       // -10 -5 1 3 3 8   -7 7
//            } else {
//                positive[i] = a[i];
//                countP++;
//            }
//        }
//
//        int[] neg = new int[countN];
//        for (int i = 0; i < countN; i++) {
//            neg[i] = negative[n - countN + i];
//        }
//
//        int[] pos = new int[countP];
//        for (int i = 0; i < countP; i++) {
//            pos[i] = positive[n - countP + i];
//        }
//
//        System.out.println(Arrays.toString(neg));
//        System.out.println(Arrays.toString(pos));
//
//        int[] result = new int[n];
//        int r = 0, j = 0, count = 0;
//        for (int k = 0; k < neg.length; k++) {
//            for (j = r - k; j < pos.length; j++) {       // -10 -5 1 3 3 8  |  -7 7  | -20 -16 -10 0 1  |   -2 0 1 2 3  |   -9 -8 -6 -4 -2 0
//                if (neg[k] > pos[j]) {                  // (12) -9 -8 -6 -4 -2 0 1 2 3 4 5 6    // -6 -5 -4 -3 -2 -1
//                    result[r++] = pos[j];
//                    count++;
//                } else {
//                    if (neg[k] < pos[j]) {
//                        result[r++] = neg[k];
//                        count++;
//                        if (k == neg.length - 1) {
//                            for (int i = j + 1; i < pos.length; i++) {
//                                result[r++] = pos[i];
//                                count++;
//                            }
//                            break;
//                        }
//                        break;
//                    }
//
//                    if (neg[k] == pos[j]) {
//                        result[r++] = pos[j];
//                        count++;
//                        result[r++] = pos[j];
//                        k++;
//                        count++;
//                        if (k == neg.length - 1) {
//                            for (int i = j + 1; i < pos.length; i++) {
//                                result[r++] = pos[i];
//                                count++;
//                            }
//                            break;
//                        }
//                    }
//                }
//            }
//
//            if (j == pos.length) {
//                for (int i = k + j; i < n; i++) {
//                    result[r++] = neg[k++];
//                    count++;
//                }
//            }
//
//        }
//
//        System.out.println(Arrays.toString(result));
//        System.out.println("count = " + count);
//        for (int i = 0; i < n; i++) {
//            System.out.print(result[i] * result[i] + " ");
//        }

////////////////////////    3.  Counting Sort
//
//        int max = Math.max(Math.abs(a[0]), Math.abs(a[n - 1]));
//
//        for (int i = 0; i < a.length; i++) {
//            a[i] = Math.abs(a[i]);
//        }
//
//        int[] numCounts = new int[max + 1];
//        for (int num : a) {
//            numCounts[num]++;   // Накопление количества элементов исходного
//        }
//
//        int[] sortedArray = new int[n];
//        int sortIndex = 0;
//
//        for (int i = 0; i < numCounts.length; i++) {
//            int count = numCounts[i];
//            for (int j = 0; j < count; j++) {
//                sortedArray[sortIndex] = i;                // количество значений
//                sortIndex++;
//            }
//        }
//
//        for (int i = 0; i < n; i++) {
//            sortedArray[i] = sortedArray[i] * sortedArray[i];
//            System.out.print(sortedArray[i] + " ");
//        }
///////////////////////////
    }
}

// -10 -5 1 3 3 8   -7 7