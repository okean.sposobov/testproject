package dz.dz2_1_dop;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        System.out.println(getPassword());
    }

    static String getPassword() {
        // Ввод количества символов в пароле и проверка длины
        Scanner scanner = new Scanner(System.in);
        int n;
        while (true) {
            n = scanner.nextInt();
            if (n < 8) {
                System.out.println("Пароль с " + n + " количеством символов небезопасен");
            } else break;
        }

        // Заполнение пароля
        char[] ch = new char[n];
        char[] nSet = getNecessarySet();
        System.arraycopy(nSet, 0, ch, 0, nSet.length);
        for (int i = nSet.length; i < n; i++) {
            ch[i] = getSymbol();
        }

        // Перемешивание символов пароля
        char[] chShuffle = new char[n];
        for (int i = 0; i < n; i++) {
            int pos = (int) (Math.random() * (n - 1));
            System.arraycopy(ch, pos + 1, chShuffle, 0, n - pos - 1);
            System.arraycopy(ch, 0, chShuffle, n - pos - 1, pos + 1);
        }

        return new String(chShuffle);
    }


    static char[] getNecessarySet() {
        char[] symbols = new char[4];
        char[] spec = {'_', '*', '-'};
        symbols[0] = (char) ((int) (Math.random() * 26) + 65);
        symbols[1] = (char) ((int) (Math.random() * 26) + 97);
        symbols[2] = (char) ((int) (Math.random() * 10) + 48);
        symbols[3] = spec[(int) (Math.random() * 3)];

        return symbols;
    }

    static char getSymbol() {
        return getNecessarySet()[(int) (Math.random() * 4)];
    }
}
