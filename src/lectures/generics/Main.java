package lectures.generics;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        List<A> listA = new ArrayList<>();
        listA.add(new A());
        listA.add(new A());

        List<B> listB = new ArrayList<>();
        listA.add(new B());
        listA.add(new B());

        List<? extends A> list1 = listA; // неизвестно какой точно тип внутри list1 (какой-то производный от А), поэтому list1 стала неизменяемой, т.к. добавить можно только известный тип
//        List<? extends A> list1 = listB;    // В производный от А
        // Создается producer
        list1 = listB; // list1 стала неизменяемой, но данные можно прчитать - получается producer

        fooExtends(list1);

        List<? super A> list2 = listA;
    }

    // Создается producer
    private static void fooExtends(List<? extends A> list) { // неизвестно какой точно тип внутри (какой-то производный от А), поэтому list1 стала неизменяемой, т.к. добавить можно только известный тип
//        list.add(new A());    // Ошибка компиляции,
//        list.add(new B());    // Ошибка компиляции т.к. нельзя добавить ничего из-за неизвестности точного типа в list
        A a = list.get(0);  // получить элемент можно. Создан продьюсер.
    }

// Consumer (Добавлять можем, а получать только Object)
    private static void fooSuper(List<? super B> list) { // Можно эту коллекцию менять, но получить можно только Object
        list.add(new B());
        list.add(new C());  // Можно добавить наследников от В , т.к. тип коллекции <? super B> (например Object) будет базовым для наследников от В
//        list.add(new Object());  //  Ошибка компиляции, т.к. можно добавить только В или наследников от В

//        A a = list.get(0); // Ошибка компиляции, т.к. неизвестно каким точно типом типизирована коллекция, тип может быть B, A или Object
//        B b = list.get(0); // Ошибка компиляции, т.к. неизвестно каким точно типом типизирована коллекция, тип может быть B, A или Object
        Object o = list.get(0); // Можно Object , т.к. базовый для всех

    }
}
