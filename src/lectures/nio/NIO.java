package lectures.nio;

import java.nio.file.Path;
import java.nio.file.Paths;

public class NIO {
    public static void main(String[] args) {
        Path path1 = Paths.get("S:/test/hello.java");
        Path path2 = Paths.get("S:/test/work");

        Path relativePath = path2.relativize(path1); // получить путь из S:/test/work до S:/test/hello.java , получается относительный путь
        System.out.println("relativePath=" + relativePath); // relativePath=..\hello.java  .. (две точки означает подняться на уровень выше)

        Path pathToFileHelloJava = path2.resolve(relativePath); // добавление относительного пути к текущему
        System.out.println("pathToFileHelloJava = " + pathToFileHelloJava); // pathToFileHelloJava = S:\test\work\..\hello.java
        // означает заход в S:\test\work, \..\ на уровень выше, получаем hello.java
        // далее нужно нормализовать pathToFileHelloJava
        Path normalisedPath = pathToFileHelloJava.normalize();
        System.out.println("normalisedPath = " + normalisedPath); // normalisedPath = S:\test\hello.java

    }
}
