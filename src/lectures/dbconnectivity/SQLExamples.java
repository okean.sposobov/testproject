package lectures.dbconnectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLExamples {
    static String postgresqlDriver = "org.postgresql.Driver";
    static String postgresqlURL = "jdbc:postgresql://localhost:5432/"; // /test_db

    public static void main(String[] args) {
        String db = "test1";
//        try (Connection connection = DriverManager.getConnection(postgresqlURL, "postgres", "q1684q")) {
//            System.out.println(connection);
//            createDB(connection, db);
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
        try (Connection connection = DriverManager.getConnection(postgresqlURL + db, "postgres", "q1684q")) {
// CREATE TABLE
            //language=SQL
//            String s1 =
//                    "CREATE TABLE students(" +
//                            "id INT," +
//                            "name VARCHAR(50)," +
//                            "surname VARCHAR(50)," +
//                            "email VARCHAR(50)," +
//                            "mobile VARCHAR(50)" +
//                            ");";
//            connection.prepareStatement(s1).execute();
            //language=SQL
            String s2 = "" +
                    "ALTER TABLE students ADD COLUMN birthday DATE;";
            connection.prepareStatement(s2).execute();

        } catch (
                SQLException e) {
            throw new RuntimeException(e);
        }


    }

    public static void createDB(Connection connection, String dbName) throws SQLException {
        String s = "CREATE DATABASE " + dbName;
//        System.out.println(s);
        PreparedStatement preparedStatement = connection.prepareStatement(s);
        preparedStatement.execute();
    }


}
