package lectures.dbconnectivity;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    Connection connection;

    ProductRepository(Connection connection) {
        this.connection = connection;
    }

    public void createTable() throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "CREATE TABLE product (" +
                "id varchar(32), " +
                "name varchar(32), " +
                "m_date varchar(32)" +
                ");";
        int affectedRows = statement.executeUpdate(sql);
        System.out.println("CREATE TABLE complete, affectedRows=" + affectedRows);
    }

    public void dropTable() throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "DROP TABLE product;";
        int affectedRows = statement.executeUpdate(sql);
        System.out.println("DROP TABLE complete, affectedRows=" + affectedRows);
    }

    public void addProduct(Product product) throws SQLException {
        Statement statement = connection.createStatement();
        String sql = String.format("INSERT INTO product (id, name, m_date) VALUES ('%s', '%s', '%s')",
                product.getId(), product.getName(), product.getmDate().toString());
        boolean hasResultSet = statement.execute(sql);

        int affectedRows = statement.getUpdateCount();
        System.out.println("hasResultSet=" + hasResultSet + " affectedRows=" + affectedRows);
    }

    public List<Product> findAll() {
        try (Statement statement = connection.createStatement()) {
            String sql = "SELECT * FROM product;";

            try (ResultSet resultSet = statement.executeQuery(sql)) {
                List<Product> products = new ArrayList<>();
                while (resultSet.next()) {
                    String id = resultSet.getString(1); //  resultSet.getString(номер_столбца)
                    String name = resultSet.getString("name"); // resultSet.getString(имя_столбца)
                    LocalDate m_date = LocalDate.parse(resultSet.getString("m_date"));
                    products.add(new Product(id, name, m_date));
                }
                return products;
            }
        } catch (Exception ex) {
            throw new RuntimeException("Failed .findAll()" + ex.getMessage(), ex);
        }
    }

    public void deleteById(String id) {
//        Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM product WHERE id = ?")) {
            preparedStatement.setString(1, id); // 1 = номер параметра (?) в записи "DELETE FROM product WHERE id = ?"

            int affectedRows = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException("Error delete id=" + id + "error " + e.getMessage(), e);
        }

    }
}

