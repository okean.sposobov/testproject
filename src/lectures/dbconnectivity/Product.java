package lectures.dbconnectivity;

import java.time.LocalDate;

public class Product {
    private String id;
    private String name;
    private LocalDate mDate;

    public Product(String id, String name, LocalDate mDate) {
        this.id = id;
        this.name = name;
        this.mDate = mDate;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getmDate() {
        return mDate;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", mDate=" + mDate +
                '}';
    }
}
