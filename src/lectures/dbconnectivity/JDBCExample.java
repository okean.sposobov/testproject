package lectures.dbconnectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * PreparedStatement preparedStatement = connection.prepareStatement(sql); - предпочтительнее, т.к. безопаснее, не допускает sql-инъекций
 * <p>
 * Connection connection -
 * PreparedStatement preparedStatement - эти ресурсы должны быть закрыты после использования, например try() с ресурсами
 * ResultSet resultSet -
 * <p>
 * ResultSet - интерфейс, итерируемый
 * <p>
 * boolean execute(String script) для всех
 * ResultSet executeQuery(String script) для операций SELECT
 * int executeUpdate(String script) кроме SELECT
 * <p>
 * ResultSet.TYPE_FORWARD_ONLY  Просмотр ResultSet только в одном направлении
 * ResultSet.TYPE_SCROLL_INSENSITIVE  Просмотр ResultSet в двух направлениях
 * ResultSet.TYPE_SCROLL_SENSITIVE  Просмотр ResultSet чувствителен к изменению данных в БД, подтягиваются изменеия происходящие в БД
 * <p>
 * ResultSet.CONCUR_READ_ONLY Только чтение из ResultSet
 * ResultSet.CONCUR_UPDATABLE Позволяет через методы ResultSet изменять значения в БД
 */


public class JDBCExample {
    public static void main(String[] args) {
//        Class.forName("org.postgresql.Driver");
        String jdbcURL = "jdbc:postgresql://localhost:5432/test_db";
        ProductRepository productRepository;
        try (Connection connection = DriverManager.getConnection(jdbcURL, "postgres", "q1684q")) {
            System.out.println("connection: " + connection);
            productRepository = new ProductRepository(connection);
//            Product product1 = new Product("1", "Product_1", LocalDate.of(2022, 12, 31));
//            productRepository.addProduct(product1);
//            productRepository.addProduct(new Product("2", "Product_2", LocalDate.of(2023, 1, 3)));
//            productRepository.addProduct(new Product("3", "Product_3", LocalDate.of(2023, 2, 4)));
//            productRepository.deleteById("3");
            productRepository.findAll().forEach(System.out::println);

//        productRepository.createTable();
//        productRepository.dropTable();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }
}
