package lectures.innerClasses.anonimClass;

public class AnonymousClass {
    public static void main(String[] args) {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                System.out.println("in run()");
            }
        };

        task.run();

        Runnable runnable = ()->{
            System.out.println("with lambda");
        };
        runnable.run();
    }
}
