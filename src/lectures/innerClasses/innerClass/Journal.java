package lectures.innerClasses.innerClass;

import lectures.innerClasses.innerStatic.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Journal {
    private List<User> students = new ArrayList<>();

    public void addStudent(User user) {
        students.add(user);
    }

    public Iterator<User> getJournalIterator(){
        return new JournalIterator();
    }

    public class JournalIterator implements Iterator<User> {

        private int position;

        @Override
        public boolean hasNext() {
            if (position < students.size()) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public User next() {
            User user = students.get(position);
            position += 1;
            return user;
        }
    }
}
