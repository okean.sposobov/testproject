package lectures.innerClasses.innerClass;

import lectures.innerClasses.innerStatic.User;

import java.util.Iterator;

public class InnerClass {
    public static void main(String[] args) {
        Journal journal = new Journal();
        journal.addStudent(User.getBuilder().setId("1").setName("Aaaa").createUser());
        journal.addStudent(User.getBuilder().setId("2").setName("Bbbb").createUser());

        Iterator iterator = journal.getJournalIterator();

        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
