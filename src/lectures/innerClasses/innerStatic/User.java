package lectures.innerClasses.innerStatic;

public class User {
    private String id;
    private String name;

    public static Builder getBuilder() {
        return new Builder();
    }

    public static class Builder {
        private User user = new User();

        public Builder setId(String id) {
            user.id = id;
            return this;
        }

        public Builder setName(String name) {
            user.name = name;
            return this;
        }

        public User createUser() {
            if (user.id == null) {
                throw new IllegalArgumentException("id can not be null!");
            }
            return user;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
