package lectures.innerClasses.innerStatic;

public class InnerStaticBuilderPattern {
    public static void main(String[] args) {
//        User user = User.getBuilder().setName("Aaaa").createUser();
        User user1 = User.getBuilder().setId("123").setName("Aaaa").createUser();
        System.out.println(user1);

    }
}
