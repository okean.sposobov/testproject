package lectures.equalsHashCode;

/*
Рефлексивность
 obj.equals(obj) == true для любого obj != null

Симметричность
Если objA.equals(objB) == true, то objB.equals(objA) == true

Транзитивность
Если objA.equals(objB) == true, objB.equals(objC) == true, то objA.equals(objC) == true

Повторные вызовы objA.equals(objB) будут давать постоянный результат если объекты не модифицируется

Сравнение с null
obj.equals(null) == false для любого объекта obj

Контракт equals & hashCode
Если objA.equals(objB) == true, то objA.hashCode() == objB.hashCode()
Следствие
Если objA.hashCode() != objB.hashCode(), то objA.equals(objB) == false
 */

public class Equals {
    public static void main(String[] args) {
        User user1 = new User("u", "Name");
        User user2 = new User("u", "Name");

        boolean equalsValue = user1.equals(user2);
        System.out.println("equalsValue: " + equalsValue);
        System.out.println("user1.hashCode(): " + user1.hashCode());
        System.out.println("user1.hashCode(): " + user2.hashCode());

    }
}
