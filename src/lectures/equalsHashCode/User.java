package lectures.equalsHashCode;

import java.util.Objects;

public class User {
    private String login;
    private String name;

    public User(String login, String name) {
        this.login = login;
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return login.equals(user.login) && name.equals(user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, name);
//        return  login.hashCode(); // если equals() == true , то hashCode() 2-х объектов равны
//        return login.hashCode() + name.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

