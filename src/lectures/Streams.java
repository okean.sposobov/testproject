package lectures;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Streams {
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5);
        Stream<String> newStream = stream.map((Integer digit) -> { // .map(...) для преобразования одного стрима в другой
            return "str " + digit;
        });
        newStream.forEach(System.out::println);

        Stream.of(1, 2, 3, 4, 5)
                .filter((Integer digit) -> {
                    if (digit > 2) {
                        return true;
                    } else {
                        return false;
                    }
                })
                .map((Integer digit) -> {
                    return "newStr " + digit;
                })
                .forEach(System.out::println);

        List<String> list = Stream.of(1, 2, 3, 4, 5)
                .filter((Integer digit) -> {
                    if (digit > 2) {
                        return true;
                    } else {
                        return false;
                    }
                })
                .map((Integer digit) -> {
                    return "newStr " + digit;
                })
                .collect(Collectors.toList());

        List<String> list1 = Stream.of(1, 2, 3, 4, 5)
                .filter((digit) -> digit > 2)
                .map((digit) -> "newStr " + digit)
                .collect(Collectors.toList());

        List<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < 6; ++i) {
            list2.add(i);
        }
        list2
                .stream()
                .filter((digit) -> digit >= 2 && digit <= 4)
                .map((digit) -> digit * 2)
                .forEach(System.out::println);
        System.out.println(list2); // stream не меняет содержимое коллекции, на которой был вызван

        System.out.println();
        Map<Integer, Integer> map = list2
                .stream()
                .filter((digit) -> digit >= 2 && digit <= 4)
                .map((digit) -> digit * 2)
                .collect(Collectors.toMap(k -> k, a -> a * 2, Integer::sum));
        System.out.println(list2);
        System.out.println(map);

        System.out.println();
        Map<Boolean, List<Integer>> map1 = list2.stream()
                .collect(Collectors.partitioningBy((Integer digit) -> digit <= 2)); // на 2 части
        System.out.println(map1 + "\n");

        Map<Integer, List<Integer>> map2 = list2.stream()
                .collect(Collectors.groupingBy((Integer digit) -> digit % 3)); // на группы, ключ - это digit % 3, а значения
        // попадают в List<Integer> по условию % 3
        System.out.println(map2);

    }
}
