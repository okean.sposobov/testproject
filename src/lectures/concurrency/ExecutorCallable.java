package lectures.concurrency;

import java.util.concurrent.*;

public class ExecutorCallable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<String> callable = () -> {
            System.out.println(Thread.currentThread().getId() + " " + Thread.currentThread().getName() + " " + Thread.currentThread().isInterrupted());
            Thread.sleep(5000);
            System.out.println(Thread.currentThread().getId() + " " + Thread.currentThread().getName() + " " + Thread.currentThread().isInterrupted());

            return "Hello!!!";
        };

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        Future<String> future = executorService.submit(callable);
        Thread.sleep(1000);
//        future.cancel(true); // отменяет выполняемую задачу, прерывает поток
        String result = future.get(); // future.get() блокирующий вызов, пока не получен результат, дальше выполнение не происходит
        System.out.println(result);
//        executorService.shutdown(); // не останавливает текущие задачи
        executorService.shutdownNow(); // пытается завершить все задания: и ожидающие в очереди, и выполняющиеся.
        System.out.println(Thread.currentThread().getId() + " " + Thread.currentThread().getName());
    }
}
