package lectures.concurrency;

public class WaitNotify {
    public static void main(String[] args) throws InterruptedException {
        Object monitor = new Object();
        System.out.println("Main start " + "id=" + Thread.currentThread().getId() + " " + Thread.currentThread().getName());

        MyThread myThread = new MyThread(monitor);
        myThread.start();
        synchronized (monitor) {
            monitor.wait(); // команда главному потоку ждать пока не будет вызван monitor.notify() из потока, который захватил монитор
        }
        // после вызова monitor.notify() из отдельного потока главный поток продолжает выполнение
        System.out.println("Main finish " + "id=" + Thread.currentThread().getId() + " " + Thread.currentThread().getName());

    }
}

class MyThread extends Thread {
    Object monitor;

    MyThread(Object monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {
        System.out.println("start " + "id=" + Thread.currentThread().getId() + " " + Thread.currentThread().getName());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("finish " + "id=" + Thread.currentThread().getId() + " " + Thread.currentThread().getName());

        synchronized (monitor) {
            monitor.notify(); // в конце выполнения задачи в отдельном потоке вызывается оповещение ждущему потоку(в данном случае главному), что нужно продожить выполнение
        }
    }
}
