package lectures.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicVars {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(); // Atomic классы как synchronized блок кода, только для одной переменной
        atomicInteger.incrementAndGet();
        System.out.println(atomicInteger);
    }
}
