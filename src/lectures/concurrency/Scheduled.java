package lectures.concurrency;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Scheduled {
    public static void main(String[] args) {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);

        Runnable runnable = () -> {

        };
        scheduledExecutorService.scheduleAtFixedRate(runnable, 1, 10, TimeUnit.SECONDS); // периодичность запуска, указывается время между запусками (10 seconds)
        scheduledExecutorService.scheduleWithFixedDelay(runnable, 1, 10, TimeUnit.SECONDS); // задержка (10 seconds) между концом завершённой задачи и началом следующей
    }
}
