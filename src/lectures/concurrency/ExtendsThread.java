package lectures.concurrency;

public class ExtendsThread {
    public static void main(String[] args) throws InterruptedException {
        MyThreadClass myThreadClass = new MyThreadClass();
        myThreadClass.start();

        myThreadClass.join(); // здесь главный поток (или поток, в котором есть такой вызов) ждёт завершения задачи отдельного потока из myThreadClass
        // myThreadClass.join(1000) главный поток ждёт 1000 мсек. и продолжает выполнение

        System.out.println("Main " + "id=" + Thread.currentThread().getId() + " " + Thread.currentThread().getName());
    }
}

class MyThreadClass extends Thread {
    @Override
    public void run() {
        System.out.println("start " + "id=" + Thread.currentThread().getId() + " " + Thread.currentThread().getName());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("finish " + "id=" + Thread.currentThread().getId() + " " + Thread.currentThread().getName());

    }
}