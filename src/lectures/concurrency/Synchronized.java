package lectures.concurrency;

public class Synchronized {
    public static void main(String[] args) {
        Object object = new Object(); // монитор есть у любого объекта

        synchronized (object){ // здесь происходит захват монитора потоком, для обеспечения доступа только одним захватившим монитор потоком к критической секции,
            // вместо object может быть любой объект

        }
    }
}
