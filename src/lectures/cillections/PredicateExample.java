package lectures.cillections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

public class PredicateExample {
    public static void main(String[] args) {
        Collection<Integer> list = new ArrayList<>();

        list.add(10);
        list.add(20);
        list.add(30);

        Predicate<Integer> testFunc = (Integer digit) -> {
            if (digit < 30) {
                return true;
            } else {
                return false;
            }
        };

        list.removeIf(testFunc);
        System.out.println(list);

    }
}
