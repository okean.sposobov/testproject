package lectures.cillections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapEntry {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();

        for (int i = -2; i <= 5; ++i) {
            map.put(i, "Value_" + i);
        }
        System.out.println("map: " + map);

        Set<Map.Entry<Integer, String>> entries = map.entrySet();
        for (Map.Entry<Integer, String> entry : entries) {
            Integer key = entry.getKey();
            String value = entry.getValue();

            System.out.println("{" + key + ";" + value + "}");
        }
    }
}
