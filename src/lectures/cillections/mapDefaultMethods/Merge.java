package lectures.cillections.mapDefaultMethods;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Merge {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("key1", 123);

        BiFunction <Integer, Integer, Integer> mergeFunction = (Integer oldValue, Integer newValue) -> {
            System.out.println("Inside mergeFunction: oldValue = " + oldValue + " newValue = " + newValue);
            return oldValue + newValue;
        };

        // метод для добавления, если произойдёт коллизия по ключам при добавлении
        Integer result = map.merge("key1", 500, mergeFunction);
        System.out.println("result = " + result);
        System.out.println("map: " + map);
    }
}
