package lectures.cillections.mapDefaultMethods;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Compute {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("key1", 123);

        BiFunction<String, Integer, Integer> computeFunction = (String key, Integer oldValue) -> {
            System.out.println("Inside computeFunction: key = " + key + " oldValue = " + oldValue);
            return oldValue * 2;
        };

// вычисляет значение на лету и при коллизиях по ключу,
        Integer result = map.compute("key1", computeFunction);
//
//        map.computeIfAbsent() - если записи нет
//        map.computeIfPresent() - если запись есть

        System.out.println("result = " + result);
        System.out.println("map: " + map);


    }
}
