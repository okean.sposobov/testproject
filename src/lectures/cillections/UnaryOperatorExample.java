package lectures.cillections;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class UnaryOperatorExample {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 1; i <= 5; i++) {
            list.add(i);
        }
        System.out.println("list: " + list);

        UnaryOperator<Integer> uop = (Integer digit) -> {
            return -1 * digit;
        };

        list.replaceAll(uop);

        System.out.println("list: " + list);

    }
}
