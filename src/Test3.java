import java.util.Arrays;

public class Test3 {
    public static void main(String[] args) {
        int[] oldList = {1, 2, 3, 5, 4};
        reverse(oldList);
        //Arrays.sort(oldList);
        for (int i = 0; i < oldList.length; i++)
            System.out.print(oldList[i] + " ");
    }

    public static void reverse(int[] list) {
        int[] newList = new int[list.length];

        for (int i = 0; i < list.length; i++)
            newList[i] = list[list.length - 1 - i];
        for (int i = 0; i < list.length; i++) {
            list[i] = newList[i];

        }

        list = newList;
    }
}