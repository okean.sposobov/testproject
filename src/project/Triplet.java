package project;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Triplet<A extends Comparable<A>> {
    private final A first;
    private final A second;
    private final A third;
    private final List<A> valueList;

    public Triplet(A first, A second, A third) {
        this.first = first;
        this.second = second;
        this.third = third;
        valueList = Arrays.asList(first, second, third);
    }

    public A getFirst() {
        return first;
    }

    public A getSecond() {
        return second;
    }

    public A getThird() {
        return third;
    }

    @Override // Переопределяет метод toString класса Object
    public String toString() {
        return valueList.toString();
    }

    /** Проверить наличие элемента в кортеже */
    public final boolean contains(A value) {
        return valueList.contains(value);
    }

    /** Получить минимальный элемент кортежа */
    public final A min() {
        return Collections.min(valueList);
    }

    /** Получить результат сложения элементов кортежа */
    public final String sum() {
        if (getFirst() instanceof Number
                && getSecond() instanceof Number
                && getThird() instanceof Number) {
            double sum = ((Number) getFirst()).doubleValue() +
                    ((Number) getSecond()).doubleValue() +
                    ((Number) getThird()).doubleValue();
            return Double.toString(sum);
        } else {
            return String.join(" ",getFirst().toString(), getSecond().toString(), getThird().toString());
        }
    }


}
