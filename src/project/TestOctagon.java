package project;

public class TestOctagon {
    public static void main(String[] args) {
        // Создать новый объект класса Octagon с заданной стороной
        Octagon originalOctagon = new Octagon(5);
        // Отобразить информацию о площади
        System.out.println("Площадь равна: " + originalOctagon.getArea());
        // Отобразить информацию о периметре
        System.out.println("Периметр равен: " + originalOctagon.getPerimeter());

        try {
            Octagon cloneOctagon = (Octagon) originalOctagon.clone();
            // Сравнить два объекта
            int comparisonResult = originalOctagon.compareTo(cloneOctagon);
            // Отобразить результат сравнения двух объектов
            System.out.println("Результат сравнения объектов: " + comparisonResult);
        } catch (CloneNotSupportedException e) {
            System.out.println("Объект не может быть клонирован");
            throw new RuntimeException(e);
        }

    }
}
