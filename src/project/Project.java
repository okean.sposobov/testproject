package project;

import java.util.Scanner;

public class Project {

    static final double ROUBLES_PER_YUAN = 8.35;

    public static void main(String[] args) {

        double[] roubles;
        double[] yuans;
        int n;
        Scanner input = new Scanner(System.in);

        // Отобразить инструкцию
        instruct();

        // Ввести корректное число конвертаций
        do {
            System.out.print("Введите коррктное количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);
        yuans = new double[n];

        // Получить n сумм денег в юанях
        System.out.println("Введите " + n + " 3сумм денег в юанях через пробел");
        for (int i = 0; i < n; i++) {
            yuans[i] = input.nextDouble();
        }

        // Конвертировать в российские рубли
        roubles = find_roubles(yuans);

        // Отобразить таблицу с результатами
        System.out.println("\tСумма, ¥\tСумма, $");
        for (int i = 0; i < n; i++) {
            System.out.println("\t" + yuans[i] + "\t\t" + ((int) (roubles[i] * 100)) / 100.0);
        }


        //System.out.println(Math.floor((roubles * 100)) / 100.0 + " российского рубля");


    }




    /**
     * Отображает инструкцию
     */
    public static void instruct() {
        System.out.println("Эта программа конвертирует суммы денег из китайских юаней в российские рубли.");
        System.out.println("Курс покупки юаней равен " + ROUBLES_PER_YUAN + " рубля.");

    }

    /**
     * Конвертирует сумму денег из китайских юаней в российские рубли.
     */
    public static double[] find_roubles(double[] yuansArray) {
        double roublesArray[] = new double[yuansArray.length];
        for (int i = 0; i < yuansArray.length; i++) {
            roublesArray[i] = ROUBLES_PER_YUAN * yuansArray[i];
        }

        return roublesArray;
    }
}


//        for (int i = 0; i < conversionsNumber; ++i) {
//            System.out.print("Введите сумму денег в юанях: ");
//            yuans = input.nextDouble();
//            System.out.print(yuans);
//
//            if (5 <= yuans && yuans <= 20) {
//                System.out.print(" юаней равны ");
//            } else {
//                digit = (int) yuans % 10;
//                if (digit == 1) {
//                    System.out.print(" юань равен ");
//                } else {
//                    if (2 <= digit && digit <= 4) {
//                        System.out.print(" юаня равны ");
//                    } else {
//                        System.out.print(" юаней равны ");
//                    }
//                }
//            }


