package project;

import static java.lang.Math.sqrt;

public class Octagon extends GeometricObject implements Comparable<Octagon>, Cloneable{
    private double side;

    public Octagon() {
        this.side = 0;
    }

    public Octagon(double side) {
        this.side = side;
    }

    /** Возвращает длину стороны */
    public double getSide() {
        return side;
    }

    /** Присваивает длину стороны */
    public void setSide(double side) {
        this.side = side;
    }

    /** Возвращает площадь */
    @Override
    public double getArea() {
        return (2 + 4 / sqrt(2)) * side * side;
    }

    /** Возвращает периметр */
    @Override
    public double getPerimeter() {
        return 8 * side;
    }

    /** Реализует метод интерфейса Comparable */
    @Override
    public int compareTo(Octagon o) {
        return Double.compare(this.side, o.side);
    }

    /** Переопределяет метод класса Object */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
