import java.io.IOException;
import java.nio.file.*;

/**
 * !!! ВНИМАНИЕ !!!
 * Программа тупая!!!
 * Инструкция (читать до конца)
 * Создайте папку на диске типа D:\Projects\Tmp или D:\Bot.
 * Скопируте в эту папку файлы решенных задач из своего проекта (например  ..Project\src\dz\dz1_2).
 * Получится что-то типа
 * Task1
 * Task2
 * ...
 * Task11
 * ...
 * Добавьте путь к созданной Вами папке в строку     Path path = Paths.get("");    между двойных кавычек.
 * Получится что-то типа
 * Path path = Paths.get("D:\\Bot");
 * Запустите программу из IDE (IntelliJ IDEA).
 * В созданной Вами папке появится подпапка \Solutions,
 * в ней будут подпапки с именами файлов решенных задач,
 * в которых будет отредактированный (удалена строка с package и переименован класс) файл Solution.java.
 * Отправляйте файл Solution.java из соответствующей подпапки.
 * !!!!!!          ВАЖНО              !!!!!!
 * После отправки всех файлов очистите всю созданную Вами папку полностью до состояния как сразу после создания,
 * чтобы программа могла работать с этой папкой при копировании в неё новой порции файлов.
 */
public class ReName {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("D:\\Bot");                           ///////////// Добавлять путь здесь //////////
        Path solutions = Paths.get(path.toString() + "\\Solutions");
        Path task, solution;
        String taskName;
        int count = 0;

        Files.createDirectory(solutions);
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path p : stream) {
                if (Files.isDirectory(p)) continue;
                taskName = p.getFileName().toString().replace(".java", "");
                task = Paths.get(solutions.toAbsolutePath() + "\\" + taskName);
                Files.createDirectory(task);
                solution = Paths.get(task + "\\" + "Solution.java");
                Files.copy(p, solution);
                Files.write(solution, Files.readString(solution)
                                .replace(taskName, "Solution").replaceAll("package.*", "")
                                .getBytes()
                        , StandardOpenOption.TRUNCATE_EXISTING
                );
                count++;
            }
            System.out.println("Обработано " + count + " файлов");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
