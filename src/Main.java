import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

//        final int i;
//        i = 2;
//        System.out.println("Hello world! " + i);
//        System.out.println(3 % 7);

        //double x1 = 0.0, x2 = 0.0, x3 = 0.0;
//        Scanner input = new Scanner(System.in);
//        double x1 = input.nextDouble();
//        double x2 = input.nextDouble();
//        double x3 = input.nextDouble();

        //System.out.println(Math.pow(x1 * x2 * x3, 1.0/3));
        int j = 1;
//        System.out.println(j++);
//        System.out.println(j);

//        Начало: 13505
//        Конец:  13810


        int start, end, distance;
        final double COST_PER_KM = 8.50;
        double finalPrice;
        Scanner input;

        input = new Scanner(System.in);

        System.out.println("СЧЕТЧИК СТОИМОСТИ ПРОЕЗДА");

        System.out.print("Введите начальные показания одометра: ");
        start = input.nextInt();

        System.out.print("Введите конечные показания одометра: ");
        end = input.nextInt();

        distance = end - start;
        finalPrice = COST_PER_KM * distance;

        System.out.println("Вы проехали " + String.valueOf(distance) + " км." + " Из расчета 8 руб. 50 коп. за км,\n" +
                "стоимость проезда равна " + (long)Math.floor(finalPrice) + " руб. " +  (int)((finalPrice - Math.floor(finalPrice)) * 100)  + " коп.");
    }
}