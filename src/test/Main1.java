package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main1 {
    public static void main(String[] args) {
//        System.out.println(Arrays.toString(IC.class.getInterfaces()));
        System.out.println();
//        System.out.println(getSuperInterfaces(IC.class));
        getSuperInterfaces(IIIC.class);
        System.out.println();
        System.out.println();
//        System.out.println(getSuperInterfaces(IC.class));
//        Class<?> cls = C.class;
//        List<Class<?>> interfaces = new ArrayList<>();
//        while (cls != Object.class) {
//            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
//            cls = cls.getSuperclass();
//        }
//        System.out.println(interfaces);
    }

    public static List<Class<?>> getSuperInterfaces(Class<?> intrfaze) {
        Class<?>[] iArr = intrfaze.getInterfaces();
        if (iArr.length == 0) {
            return new ArrayList<>();
        }

        System.out.println(intrfaze + " super: \n" + Arrays.toString(iArr));

        List<Class<?>> list = new ArrayList<>();
        for (Class<?> inf : iArr) {
            list.add(inf);
            list.addAll(getSuperInterfaces(inf));
        }

        return list;
    }
}


